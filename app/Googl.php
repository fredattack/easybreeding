<?php
/**
 * Created by PhpStorm.
 * User: fredm
 * Date: 27-08-18
 * Time: 08:44
 */

namespace App;
use Illuminate\Support\Facades\Log;


class Googl
{
    public function client()
    {
        Log::info('dans Googl');
        $client = new \Google_Client();
        Log::info('dans Googl -> new client');

        $client->setClientId(env('GOOGLE_CLIENT_ID'));
        $client->setClientSecret(env('GOOGLE_CLIENT_SECRET'));
        $client->setRedirectUri(env('GOOGLE_REDIRECT_URL'));
        $client->setScopes(explode(',', env('GOOGLE_SCOPES')));
        $client->setApprovalPrompt(env('GOOGLE_APPROVAL_PROMPT'));
        $client->setAccessType(env('GOOGLE_ACCESS_TYPE'));
        return $client;
    }
}