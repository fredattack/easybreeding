<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{

    protected $table = 'order';
    public $timestamps = true;

    protected $fillable = array('name');
    protected $visible = array('name');


    /*
        * Mise en place des relations de la classe Order
        *
        * */
    public function familles()
    {
        return $this->hasMany('famille', 'id');
    }

}