<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;


class Cage extends Model 
{

    protected $table = 'cages';
    public $timestamps = true;
    protected $fillable = array('name','zoneId', 'long', 'large', 'height','userId','description');
    protected $visible = array('name','zoneId', 'long', 'large', 'height','userId','description');

    /*
    * Mise en place des relations de la classe Bird
    *
    * */
    public function zone()
    {
        return $this->belongsTo(Zone::class, 'zoneId','id');
    }

    /**retourne une cage
     * @param $id
     * @return mixed
     */
    public static function getModel($id){
        $cage=Cage::where('id',$id)->first();
        return $cage;
    }

    /** Creer une cage
     * @param $request
     * @return bool
     */
    public static function createModel($request){
            $newCage = Cage::create([
                'name'   => $request['name'],
                'zoneId'    => $request['zoneId'],
                'long'    => $request['long'],
                'large'    => $request['large'],
                'height'    => $request['height'],
                'description'    => $request['description'],
                'userId'       => Auth::id()
                ]);

            return true;
    }

    /**retourne les cages d'un user
     * @return Cage[]|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public static function getAllOfUser()
    {
        $list = Cage::with('zone')
                    ->where('userId','=',Auth::id())
                    ->get();
        return $list;
    }

    /**Mise à jour d'une cage $id
     * @param $id
     * @param $request
     * @return mixed
     */
    public static function updateModel($id,$request)
    {
      if(Cage::where( 'id', $id)->update([
            'name' => $request->name,
            'long' => $request->long,
            'large' => $request->large,
            'height' => $request->height,
            'description' => $request->description,
            'zoneId' => $request->zoneId,
        ])) return Cage::getModel($id);
    }
}