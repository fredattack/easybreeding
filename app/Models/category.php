<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;


class category extends Model
{
    protected $visible = ['id', 'name', 'userId'];
    protected $fillable = ['id', 'name', 'userId'];

    /*
    * Mise en place des relations de la classe Categorie
    *
    * */
    public function task()
    {
        return $this->hasMany(Task::class, 'categoryId', 'id');
    }

    /**Retourne la liste des catégories d'un user
     * @return mixed
     */
    public static function getAllOfUser()
    {
        $categories = category::where('userId', 0)->orWhere('userId', Auth::id())->get();
        return $categories;
    }

    /**Creer une catégorie
     * @param $request
     * @return bool
     */
    public static function createModel($request)
    {
        if (category::create([
            'name' => $request['name'],
            'userId' => Auth::id(),
        ])) return true;
    }

    /**Retourne uen catégorie
     * @param $id
     * @return mixed
     */
    public static function getModel($id)
    {
        $cat = category::where('id', $id)->first();
        return $cat;
    }

    /**Effacer une catégorie
     * @param $id
     * @return mixed
     */
    public static function deleteModel($id)
    {
        $cat = category::where('id', $id)->first();
        $name = $cat->name;
        $cat->delete();
        return $name;
    }
}
