<?php

namespace App\Models\Auth;

use Illuminate\Support\Facades\Auth;
use App\Models\Traits\Uuid;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Notifications\Notifiable;
use App\Models\Auth\Traits\Scope\UserScope;
use App\Models\Auth\Traits\Method\UserMethod;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Auth\Traits\SendUserPasswordReset;
use App\Models\Auth\Traits\Attribute\UserAttribute;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Models\Auth\Traits\Relationship\UserRelationship;

/**
 * Class User.
 */
class User extends Authenticatable
{
    use HasRoles,
        Notifiable,
        SendUserPasswordReset,
        SoftDeletes,
        UserAttribute,
        UserMethod,
        UserRelationship,
        UserScope,
        Uuid;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'adress',
        'phone',
        'facebook',
        'instagram',
        'twitter',
        'password',
        'password_changed_at',
        'active',
        'confirmation_code',
        'confirmed',
        'timezone',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    /**
     * @var array
     */
    protected $dates = ['deleted_at','created_at','updated_at'];

    /**
     * The dynamic attributes from mutators that should be returned with the user object.
     * @var array
     */
    protected $appends = ['full_name'];

    public static function getSettings(){
        $settings=User::where('id','=',Auth::id())
                ->first()->settings;
        return $settings;
    }

    public static function getAuth(){
    $user=User::where('id','=',Auth::id())
        ->first();
//        dd($user);
    return $user;
}

    public static function updateModel($id,$data){
        if(User::where('id',$id)->update([
            'first_name'   => $data->first_name,
            'last_name'   => $data->last_name,
            'adress'    => $data->adress,
            'phone'    => $data->phone,
            'email'    =>$data->email,
            'facebook' => $data->facebook,
            'twitter' => $data->twitter,
            'instagram' => $data->instagram,
        ])) return true;
    }

    public static function updatePassword($id,$data){
        if(User::where('id',$id)->update([
            'password'   => bcrypt($data->password)
        ])) return true;
    }

    public static function updateProfilImage($id,$data){

        if(User::where('id',$id)->update([
            'profilImage'   => ($data)
        ])) return true;
    }
}
