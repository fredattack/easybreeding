<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Egg;
use App\Couple;

class Hatching extends Model
{

    protected $table = 'hatchings';
    public $timestamps = true;
    protected $dates = ['created_at', 'updated_at'];

    /*
        * Mise en place des relations de la classe Hatching
        *
        * */
    public function couple()
    {
        return $this->belongsTo(Couple::class, 'couple_id', 'id');
    }

    public function eggs()
    {
        return $this->hasMany(Egg::class, 'idHatching', 'id');
    }

    /**retourne la liste des couvées en cours
     * @param $id
     * @return Hatching|\Illuminate\Database\Eloquent\Builder|Model|null|object
     */
    public static function getCoupleActive($id)
    {
        $hatching = Hatching::with('eggs')->where([
            'couple_id' => $id,
            'status' => '1'])->first();
        return $hatching;
    }

    /** retourne une couvée $id
     * @param $id
     * @return Hatching|\Illuminate\Database\Eloquent\Builder|Model|null|object
     */
    public static function getModel($id)
    {
        $hatching = Hatching::with('eggs')->where('id', $id)->first();
        return $hatching;
    }


}