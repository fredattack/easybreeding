<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Picture extends Model
{
    protected $table = 'pictures';
    protected $fillable = ['id', 'birdId', 'imageUrl', 'specieId'];

    /*
    * Mise en place des relations de la classe Picture
    *
    * */
    public function bird()
    {
        return $this->hasOne(Bird::class, 'BirdId', 'id');
    }

    /** Creer une nouvelle picture pour un Bird
     * @param $id
     * @param $path
     * @return mixed
     */
    public static function createModel($id, $path)
    {
        $picture = Picture::create([
            'birdId' => $id,
            'imageUrl' => $path,
        ]);

        return $picture->id;
    }

    /** Creer une nouvelle picture pour une espece
     * @param $id
     * @param $path
     * @return mixed
     */
    public static function createModelSpecie($id, $path)
    {

        $picture = Picture::create([
            'specieId' => $id,
            'imageUrl' => $path,
        ]);

        return $picture->id;
    }

    /** Retourner une image $id
     * @param $id
     * @return mixed
     */
    public static function getModel($id)
    {
        $picture = Picture::where('id', $id)->first();
        return $picture;
    }

    /** Retourner toutes les images d'un oiseau
     * @param $id
     * @return mixed
     */
    public static function getBirdPicture($id)
    {
        $picture = Picture::where('birdId', $id)->first();
        return $picture;
    }
}
