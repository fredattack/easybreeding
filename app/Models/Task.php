<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Task extends Model
{
    //
    protected $fillable = ['name', 'description', 'startDate','endDate','categoryId','allDay','userId'];
    protected $visible = ['name', 'description', 'startDate','endDate','categoryId','allDay','userId'];

    /*
    * Mise en place des relations de la classe Task
    *
    * */
    public function category()
    {
        return $this->belongsTo(category::class, 'categoryId','id');
    }

    public static function getAllofUser(){
        $tasks=Task::with('category')->where('userId','=',Auth::id())->get();
        return $tasks;
    }

    /**Ajouter une TASK
     * @param $data
     * @return bool
     */
    public static function createModel($data){
        if($data->allDay){
            $date=Carbon::createFromFormat('d/m/Y',$data->startDate);
            $startDate=$date->startOfDay();
            $date=Carbon::createFromFormat('d/m/Y', $data->endDate)->addDay();
            $endDate=$date->startOfDay();

            $allDay='1';
        }
        else{
            $date=Carbon::createFromFormat('d/m/Y', $data->startDate);
            $dt=$date->startOfDay();
            $startHour=substr($data->startTime, 0, 2);
            $startMin=substr($data->startTime, 3, 2);
            $startDate=$dt->addHours($startHour)->addMinutes($startMin);

            $date=Carbon::createFromFormat('d/m/Y', $data->endDate);
            $dt=$date->startOfDay();
            $endHour=substr($data->endTime, 0, 2);
            $endMin=substr($data->endTime, 3, 2);
            $endDate=$dt->addHours($endHour)->addMinutes($endMin);

            $allDay='0';
        }
            $task = Task::create([
                'name'   => $data->name,
                'startDate'    => $startDate,
                'endDate'    => $endDate,
                'allDay'    =>$allDay,
                'categoryId'    => $data->categoryId,
                'userId'    => Auth::id(),
            ]);
            if(\Setting::get('calendar.id')!=null)  {
               $newId = app('App\Http\Controllers\Frontend\App\gCalendarController')->addEvent($task);
                $task->event_google_id = $newId;
                $task->save();
            }
            return true;
    }

    /** Mettre à jour unE TASK
     * @param $data
     * @return bool
     */
    public static function updateModel($data){
        if($data->allDay){
            $date=Carbon::createFromFormat('d/m/Y',$data->startDate);
            $startDate=$date->startOfDay();
            $date=Carbon::createFromFormat('d/m/Y', $data->endDate)->addDay();
            $endDate=$date->startOfDay();

            $allDay='1';
        }
        else{
            $date=Carbon::createFromFormat('d/m/Y', $data->startDate);
            $dt=$date->startOfDay();
            $startHour=substr($data->startTime, 0, 2);
            $startMin=substr($data->startTime, 3, 2);
            $startDate=$dt->addHours($startHour)->addMinutes($startMin);

            $date=Carbon::createFromFormat('d/m/Y', $data->endDate);
            $dt=$date->startOfDay();
            $endHour=substr($data->endTime, 0, 2);
            $endMin=substr($data->endTime, 3, 2);
            $endDate=$dt->addHours($endHour)->addMinutes($endMin);

            $allDay='0';
        }


        Task::where('id',$data->id)->update([
            'name'   => $data->name,
            'startDate'    => $startDate,
            'endDate'    => $endDate,
            'allDay'    =>$allDay,
            'categoryId'    => $data->categoryId,
            'userId'    => Auth::id(),
        ]);
             if(\Setting::get('calendar.id')!=null)  {
            $task = Task::where('id',$data->id)->first();
            app('App\Http\Controllers\Frontend\App\gCalendarController')->editEvent($task);
             }
        return true;

    }

    /** Effacer une TASK
     * @param $id
     * @return bool
     */
    public static function deleteModel($id){

        Task::destroy($id);
        return true;
    }


}
