<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Hatching;
use Illuminate\Support\Facades\Auth;

class Egg extends Model
{

    protected $table = 'eggs';
    public $timestamps = true;
    protected $fillable = array('layingDate', 'position', 'type', 'state', 'comment', 'idHatching');
    protected $visible = array('layingDate', 'position', 'type', 'state', 'comment', 'idHatching');
    protected $dates = ['created_at', 'updated_at', 'layingDate'];

    /*
        * Mise en place des relations de la classe Bird
        *
        * */
    public function hatching()
    {
        return $this->belongsTo(Hatching::class, 'idHatching', 'id');
    }

    /** retounre un oeuf $id
     * @param $id
     * @return Egg|\Illuminate\Database\Eloquent\Builder|Model|null|object
     */
    public static function getModel($id)
    {
        $egg = Egg::with('hatching')->where('id', $id)->first();
        return $egg;
    }

    /**Retourne les oeufs actif d'un user
     * @return mixed
     */
    public static function getAllofUser()
    {
        $eggs = Egg::whereHas('hatching', function ($query) {
            $query->whereHas('couple', function ($query) {
                $query->where('userId', '=', Auth::id());
            })->where('status', '1');
        })->where('status', '1')->get();
        return $eggs;
    }

    /**Retourne TOUS les oeufs  d'un user
     * @return mixed
     */
    public static function getAllofUserFull()
    {
        $eggs = Egg::whereHas('hatching', function ($query) {
            $query->whereHas('couple', function ($query) {
                $query->where('userId', '=', Auth::id());
            });
        })->get();
        return $eggs;
    }

    /**
     * retourn la liste des oeufs éclos avant la $date
     * @param $idHatchings
     * @param $date
     * @return mixed
     */
    public static function getOlderEggs($idHatchings, $date)
    {
        $eggs = Egg::where('idHatching', $idHatchings)->where('layingDate', '>', $date)->get();
        return $eggs;
    }

    /** Mettre a jour la position d'un oeuf
     * @param $id
     * @param $position
     * @return bool
     */
    public static function updatePositon($id, $position)
    {

        Egg::where('id', $id)->update([
            'position' => $position + 1,
        ]);
        return true;
    }

}