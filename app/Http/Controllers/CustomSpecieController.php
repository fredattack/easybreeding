<?php

namespace App\Http\Controllers;

use App\BirdsAlpha;
use App\Couple;
use App\CustomSpecie;
use App\Nestling;
use App\Order;
use App\Picture;
use App\Specie;
use App\Famille;
use App\Bird;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;


class CustomSpecieController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function updatecustomid()
    {
        $species= Specie::where('customId',null)->get();
        $cpt=0;
        foreach($species as $specie){
            $specie->customId=$specie->id;
            $cpt++;
            $specie->save();
        }

        echo 'fin pour '.$cpt.' espéces';
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

        $id=$request->specieId;
        Log::info('Start Update***************************** ');
        Log::info('for specieID: '.$request->specieId);
        Log::info('Request: '.json_encode($request->all()));
        Log::info('******************************************************************** ');

        $idUser=Auth::id();
        $newId=$id.'_'.$idUser;
        Log::info('NewId: '.$newId);

        if(!str_contains($id, '_')){
        Log::info('Specie is Not Custom');
          $specie  = new CustomSpecie();
          $this->updateBirds($id,$newId);
          $this->updateCouples($id,$newId);
          $this->updateNestlings($id,$newId);

          $specie->customId=$newId;
          $specie->idReferedSpecies=$id;
          $specie->idUser=$idUser;
        }
        else{
            Log::info('Specie already is Custom');
            $specie  = CustomSpecie::where('customId',$id)->firstOrFail();
        }
        $specie->commonName=$request->commonName;
        $specie->scientificName=$request->scientificName;
        $specie->incubation=$request->incubation;
        $specie->fertilityControl=$request->fertilityControl;
        $specie->girdleDate=$request->girdleDate;
        $specie->outOfNest =$request->outOfNest;
        $specie->weaning=$request->weaning;
        $specie->sexualMaturity=$request->sexualMaturity;
        $specie->spawningInterval=$request->spawningInterval;

        Log::info('Specie : '. $specie);

        if($specie->save()) $response='Update Done';

        Log::info('EndUpdate***************************** ');

        return $specie;

   }

    /**Mettre à jour l'espece d'un bird
     * @param $id
     * @param $newId
     */
  public function updateBirds($id,$newId){
        $birds=Bird::where(['species_id'=>$id,'userId' => Auth()->id()])->get();
        foreach ($birds as $bird){
            $bird->species_id=$newId;
            $bird->save();
        }

  }
  public function updateCouples($id,$newId){
        $couples=Couple::where(['specieId'=>$id,'userId' => Auth()->id()])->get();
        foreach ($couples as $couple){
            $couple->specieId=$newId;
            $couple->save();
        }

  }
  public function updateNestlings($id,$newId){
        $nestlings=Nestling::where(['species_id'=>$id,'userId' => Auth()->id()])->get();
        foreach ($nestlings as $nestling){
            $nestling->species_id=$newId;
            $nestling->save();
        }

  }

    /**retourner une espece en json pur affichage dasn une modal
     * @return \Illuminate\Http\JsonResponse
     */
    public function getSpecie(){
        $id = Input::get('id');
        Log::info('Dans Get pour'.$id.': -----------------------------');

        if(str_contains($id, '_'))
        {
            $specie=CustomSpecie::where('customId',$id)->first();
            Log::info('Is Custom: '.$specie);


            Log::info('Speciettt: '.$specie);

            if((isset($specie->idReferedSpecies))&&($specie->idReferedSpecies!=null))
            {
                $idFamille = Specie::where('id',$specie->idReferedSpecies)->first()->id_famillie;

                Log::info('idFamille: '.$idFamille);
                $famille=Famille::where('id',$idFamille)->first();
                $famillyName= $famille->name;
                $order=Order::where('id',$famille->orderId)->first();
                $orderName= $order->orderName ;
            }
            else{
                $famille="";
                $famillyName= "";
                $order="";
                $orderName= "" ;
            }
        }

        else
        {
            $specie=Specie::where('id',$id)->first();
            $specie->customId=$specie->id;
            $famille=Famille::where('id',$specie->id_famillie)->first();
            $famillyName= $famille->name;
            $order=Order::where('id',$famille->orderId)->first();
            $orderName= $order->orderName ;
        }

        if($order!='') {
            $order=Order::where('id',$famille->orderId)->first();
            $orderName= $order->orderName ;
        }
        else $orderName="";
        Log::info('OrderName: '.$orderName);

        $picture= Picture::where('specieId','=',$specie->customId)->first();

        Log::info('Specie Before Return: '.$specie);
        $data=[$orderName,$famillyName,$specie,$picture,Auth::id()];
        return response()->json($data);
    }

    /** enregistre l'image d'une espece
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function uploadSpecieImage($id,Request $request)
    {
        if($request->hasFile('file')){
            $userImage=$request->file('file');
            $imageName=$userImage->getClientOriginalName();
            $userId=Auth::id();


            $picture= Picture::where('specieId','=',$id)->first();
            if($picture==null) $idImage = Picture::createModelSpecie($id,$imageName);
            else {
                Picture::where('specieId', $id)->update([
                    'imageUrl' => $imageName
                ]);
                $public_path = public_path('/images/users/'.$userId.'/species/'.$id);
//                $this->removeDirectory($public_path);
            }
        }
            $path=$userImage->move(public_path('/images/users/'.$userId.'/species/'.$id.'/'),$imageName);
        return response()->json(['status'=>true,'Message'=>'Image uploaded','name'=>$imageName,'userId'=>Auth::id()]);

    }

    /**supprimer le dossier image d'une espèce
     * @param $path
     */
    function removeDirectory($path) {
        $files = glob(preg_replace('/(\*|\?|\[)/', '[$1]', $path).'/{,.}*', GLOB_BRACE);
        foreach ($files as $file) {
            if ($file == $path.'/.' || $file == $path.'/..') { continue; } // skip special dir entries
            is_dir($file) ? removeDirectory($file) : unlink($file);
        }
        rmdir($path);
        return;
    }


}
