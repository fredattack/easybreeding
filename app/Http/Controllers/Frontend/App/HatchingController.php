<?php 

namespace App\Http\Controllers\Frontend\App;

use App\Couple;
use App\Hatching;
use App\Models\Auth\User;
use App\Specie;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;


class HatchingController extends Controller 
{

  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public function index()
  {
//    $hatchings=Hatching::with(['couple','eggs'])->orderBy('status','desc')->get();
      $hatchings = Hatching::with(['couple','eggs'])->whereHas('couple', function ($query) {
          $query->where('userId', '=', Auth::id());
      })->get();
    $couples=Couple::getAllOfUser();
    $user=User::getAuth();

    $customSpecies = (array)Specie::getUsersSpecies();

    return view('frontend.app.hatching.hatchingsIndex',compact(['hatchings','couples','customSpecies','user']));
  }


  
}

?>