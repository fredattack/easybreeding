<?php

namespace App\Http\Controllers\Frontend\App;

use App\Models\Auth\User;
use App\Specie;
use App\Nestling;
use App\Couple;
use App\Bird;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;


class NestlingController extends Controller
{

    /**retourne la liste des oisillons
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $nestlings = Nestling::getAllofUser();
app('App\Http\Controllers\Frontend\App\BirdsController')->getRightSpecies($nestlings);
        $user = User::getAuth();

        $couples = Couple::getAllOfUser();

        return view('frontend.app.nestling.nestlingsIndex', compact(['nestlings', 'couples', 'user']));
    }

    /**Retourne un oisillon via ajax pour afficher une modal
     * @return \Illuminate\Http\JsonResponse
     */
    public function getNestling()
    {
        $id = Input::get('id');
        $nestling = Nestling::where('id', '=', $id)->first();
        $specie = Specie::where('id', '=', $nestling->species_id)->first();
        $data = [$nestling, $specie];
        return response()->json($data);
    }

    /** Creer un oisillon a partir de l'id d'un couple
     * @param $id
     * @return bool
     */
    public function createNestling($id)
    {
        $parents = Couple::where('id', $id)->first();

        Log::info('sguen :' . $parents);

        $newNestling = Nestling::create([
            'sexe' => 'unknow',
            'species_id' => $parents['specieId'],
            'father_id' => $parents['maleId'],
            'dateOfBirth' => Carbon::now('Europe/Brussels'),
            'mother_id' => $parents['femaleId'],
            'couple_id' => $parents['customId'],
            'userId' => Auth::id()
        ]);
        return true;
    }

    /** Mettre a jour un oisillon
     * @param Request $request
     * @return string
     */
    public function update(Request $request)
    {
        if (Nestling::updateModel($request)) return 'true';
        else return 'error';

    }

    /** signaler un oisillons sorti du nid et le faire passer dans la catégorie BIRD
     * @return string
     */
    public function moveOutOfNest()
    {
        $id = Input::get('id');
        $nestling = Nestling::getModel($id);
        Log:
        info('authId: ' . Auth::id());
        $newBird = Bird::create([
            'sexe' => $nestling['sexe'],
            'sexingMethode' => $nestling['sexingMethod'],
            'origin' => 'thisElevage',
            'personal_id' => $nestling['personal_id'],
            'idType' => $nestling['idType'],
            'disponibility' => 'disponible',
            'status' => 'single',
            'idNum' => $nestling['idNum'],
            'species_id' => $nestling['species_id'],
            'father_id' => $nestling['maleId'],
            'dateOfBirth' => $nestling['dateOfBirth'],
            'mother_id' => $nestling['femaleId'],
            'couple_id' => $nestling['customId'],
            'userId' => Auth::id()
        ]);
        Nestling::setOutOfNest($id);
        return 'done';
    }

    /**marquer un oiseau comme mort
     * @return string
     */
    public function setDead()
    {
        $id = Input::get('id');
        $reason = Input::get('reason');

        if (Nestling::setDead($id, $reason)) return 'done';

    }

    /********************************************
     * Description: generate stats for eggs laying for last 12 month
     * Parameters: $eggs + relations
     * Return $eggsStats
     *********************************************/
    public function generateLayingStats()
    {
        $monthsd = [];
        $cpt = 1;
        for ($i = 1; $i <= 12; $i++) {
            $ind = $i;
            Carbon::setlocale(LC_TIME, 'fr');
            $date = Carbon::now();

            $monthsd[$cpt] = $date->subMonth($ind)->format('F'); // July
            $cpt++;
        }


        $retval = [];
        $nestlingsPerMonth = array();
        for ($i = 1; $i <= 12; $i++) {
            $nestlingsPerMonth[$i] = Nestling::where('userId', Auth::id())->whereMonth('dateOfBirth', date('m', strtotime('-' . $i . ' month')))->count();

        }

        $input = array_reverse($nestlingsPerMonth);
        $month = array_reverse($monthsd);

        array_push($retval, $input);
        array_push($retval, $month);
        return $retval;
    }
}
