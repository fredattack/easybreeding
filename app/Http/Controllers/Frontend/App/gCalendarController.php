<?php

namespace App\Http\Controllers\Frontend\App;

use App\Http\Controllers\Controller;
use DateTime;


use App\Googl;




use App\Task;
use Carbon\Carbon;
use Google_Client;
use Google_Service_Calendar;
use Google_Service_Calendar_Calendar;
use Google_Service_Calendar_Event;
use Google_Service_Calendar_EventDateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Grimthorr\LaravelUserSettings\Setting;
use Session;



class gCalendarController extends Controller
{
    private $client;
    protected $service;

    public function __construct(Googl $googl)
    {
        $this->client = $googl->client();
    }

    /**
     * recupérer la liste des évenements
     */
    public function getEvent(){
        $this->addToken();
        $g_calendar_id = \Setting::get('calendar.id');; //todo modif calendar id
        $g_cal = new \Google_Service_Calendar($this->client);
        $g_calendar = $g_cal->calendars->get($g_calendar_id);
        $calendar_timezone = $g_calendar->getTimeZone();
        $params = [
            'showDeleted' => true,
            'timeMin' => Carbon::now()
                ->setTimezone($calendar_timezone)
                ->toAtomString()
        ];
        $googlecalendar_events = $g_cal->events->listEvents($g_calendar_id,$params);
        foreach($googlecalendar_events->getItems() as $event){
            if($event->id == '4ekmcht68gmnuv0ed3a4peijso')         dd($event);
        }

    }

    /**creer un nouveau calendrier
     * @return \Illuminate\Http\RedirectResponse
     */
    public function CreateCalendar()
    {
        $this->addToken();

        $title =  env('GOOGLE_CALENDAR_NAME');
        $timezone = env('APP_TIMEZONE');

        $cal = new \Google_Service_Calendar($this->client);

        $google_calendar = new \Google_Service_Calendar_Calendar($this->client);
        $google_calendar->setSummary($title);
        $google_calendar->setTimeZone($timezone);


        $created_calendar = $cal->calendars->insert($google_calendar);

         \Setting::set('calendar.id', $created_calendar->getId());
         \Setting::save();

        return redirect()->back();
    }

    /**ajouter un évenement
     * @param $task
     * @return mixed
     */
    public function addEvent($task){
        Log::info('In addEvent------'.$task->id);
        $this->addToken();

        $title=$task->name;
        $calendar_id =  \Setting::get('calendar.id');; //todo modif calendar id;
        $date=Carbon::parse($task->endDate);
        $endDate =$date;

        $dt=Carbon::parse($task->startDate);
        $startDate = $dt;


        $cal = new \Google_Service_Calendar($this->client);
        $event = new \Google_Service_Calendar_Event();

        $event->setSummary($title);
        $start = new \Google_Service_Calendar_EventDateTime();
        ($task->allDay=='1')?$start->setDate($startDate->toDateString()):$start->setDateTime($startDate->toAtomString());
        $event->setStart($start);
        $end = new \Google_Service_Calendar_EventDateTime();
        ($task->allDay=='1')?$end->setDate($endDate->toDateString()):$end->setDateTime($endDate->toAtomString());
        $event->setEnd($end);

        $created_event = $cal->events->insert($calendar_id, $event);
//        Log::info('In created_event------ '.$created_event);
        return $created_event->getId();
    }

    /**Modifier un évenement
     * @param $task
     * @return mixed
     */
    public function editEvent($task){
        $this->addToken();

        $title=$task->name;
        $calendar_id =  \Setting::get('calendar.id');; //todo modif calendar id
        $date=Carbon::parse($task->endDate);
        $endDate =$date;

        $dt=Carbon::parse($task->startDate);
        $startDate = $dt;


        $cal = new \Google_Service_Calendar($this->client);

        $event = $cal->events->get($calendar_id,$task->event_google_id);

        $event->setSummary($title);
        $start = new \Google_Service_Calendar_EventDateTime();
        ($task->allDay=='1')?$start->setDate($startDate->toDateString()):$start->setDateTime($startDate->toAtomString());
        $event->setStart($start);
        $end = new \Google_Service_Calendar_EventDateTime();
        ($task->allDay=='1')?$end->setDate($endDate->toDateString()):$end->setDateTime($endDate->toAtomString());
        $event->setEnd($end);

        $created_event = $cal->events->update($calendar_id,$task->event_google_id,$event);
        return $created_event->getId();
    }


    /**Effacer tous les évenements
     * @return \Illuminate\Http\RedirectResponse
     */
   public function deleteAllEvent(){
       $token=\Setting::get('calendar.userToken');
       Log::info('Token dasn deleteAllEvent = '.$token);
       $this->client->setAccessToken($token);

       $id =  \Setting::get('calendar.id');; //todo modif calendar id

       $service = new Google_Service_Calendar($this->client);
       $events = $service->events->listEvents($id);
       foreach ($events as $event){
           $eventId=$event->getId();
           $service->events->delete($id, $eventId);
       }
       return redirect()->route('frontend.app.settings');


   }

    /**Ajouter tous les evenement de l'utisilateur au calendrier Google
     * @return \Illuminate\Http\RedirectResponse
     */
    public function addAllEvent(){
        $token=\Setting::get('calendar.userToken');

        $this->client->setAccessToken($token);

        Log::info('In addAllEvent-------------------------------------');
        $tasks=Task::getAllofUser();
        Log::info('All tasks : ');


        foreach($tasks as $task){
            Log::info('Store tasks : ');
            $newId=$this->addEvent($task);

            if($task->event_google_id == null){
                $task->event_google_id=$newId;
                $task->save();
            }

            Log::info('**************************************************************');

        }
        return redirect()->route('frontend.app.dashboard')->with('info',trans('alerts.frontend.taskCreated'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $eventId
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function deleteEvent($eventId)
    {
        $this->addToken();

        $calendar_id = \Setting::get('calendar.id');; //todo modif calendar id;
        $cal = new \Google_Service_Calendar($this->client);
        $cal->events->delete($calendar_id,$eventId);
        return true;
    }

    /**
     * Synchroniser les calendriers
     */
    public function syncCalendar()
    {
        $this->addToken();

        $user_id = Auth::id();

       $base_timezone = env('APP_TIMEZONE');
        $sync_token = \Setting::get('calendar.sync_token');
        $g_calendar_id =\Setting::get('calendar.id');; //todo modif calendar id';

        $g_cal = new \Google_Service_Calendar($this->client);

        $g_calendar = $g_cal->calendars->get($g_calendar_id);
        $calendar_timezone = $g_calendar->getTimeZone();

        $events = Task::where('userId', '=', $user_id)
            ->pluck('event_google_id')
            ->all();

        $params = [
            'showDeleted' => true,
            'timeMin' => Carbon::now()
                ->setTimezone($calendar_timezone)
                ->toAtomString()
        ];

        if (!empty($sync_token)) {
            $params = [
                'syncToken' => $sync_token
            ];
        }
        $googlecalendar_events = $g_cal->events->listEvents($g_calendar_id, $params);

        while (true) {

            foreach ($googlecalendar_events->getItems() as $g_event) {

                $g_event_id = $g_event->id;
                $g_event_title = $g_event->getSummary();
                $g_status = $g_event->status;

                if ($g_status != 'cancelled') {
                    ($g_event->getStart()->getDateTime()==null) ? $allDay='1':$allDay='0';
                    ($g_event->getStart()->getDateTime()==null) ? $g_start=$g_event->getStart()->getDate():$g_start=$g_event->getStart()->getDateTime();
                    $g_datetime_start = Carbon::parse($g_start)
                        ->tz($calendar_timezone)
                        ->setTimezone($base_timezone)
                        ->format('Y-m-d H:i:s');
                    ($g_event->getEnd()->getDateTime()==null) ? $g_end=$g_event->getEnd()->getDate():$g_end=$g_event->getEnd()->getDateTime();
                    $g_datetime_end = Carbon::parse($g_end)
                        ->tz($calendar_timezone)
                        ->setTimezone($base_timezone)
                        ->format('Y-m-d H:i:s');

                    //check if event id is already in the events table
                    if (in_array($g_event_id, $events)) {
                        $event = Task::where('event_google_id', '=', $g_event_id)->first();
                        $event->name = $g_event_title;
                        $event->event_google_id = $g_event_id;
                        $event->startDate = $g_datetime_start;
                        $event->endDate = $g_datetime_end;
                        $event->save();
                    } else {
                        $event = new Task;
                        $event->name = $g_event_title;
                        $event->event_google_id = $g_event_id;
                        $event->startDate = $g_datetime_start;
                        $event->endDate = $g_datetime_end;
                        $event->categoryId='1';
                        $event->allDay=$allDay;
                        $event->userId=Auth::id();
                        $event->save();

                    }

                } else {
                    if (in_array($g_event_id, $events)) {
                        Task::where('event_google_id', '=', $g_event_id)->delete();
                    }
                }

            }

            $page_token = $googlecalendar_events->getNextPageToken();
            if ($page_token) {
                $params['pageToken'] = $page_token;
                $googlecalendar_events = $g_cal->events->listEvents(\setting::get('calendar.id'), $params);
            } else {
                $next_synctoken = str_replace('=ok', '', $googlecalendar_events->getNextSyncToken());
                \Setting::set('calendar.sync_token',$next_synctoken);
                \Setting::save();

                break;
            }
        }
    }

    /**
     * Ajouter le Token au client pour la connexion
     */
    public function addToken()
    {
        $token = \Setting::get('calendar.userToken');;
        Log::info('Token dasn la function = ' . $token);
        $this->client->setAccessToken($token);
//        dd($this->client);
    }
}