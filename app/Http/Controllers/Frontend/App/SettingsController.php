<?php

namespace App\Http\Controllers\Frontend\App;

use App\category;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Controller;
use JavaScript;
use Illuminate\Support\Facades\Auth;
use App\Bird;
use App\Egg;
use App\Nestling;
use App\Models\Auth\User;
use App\Googl;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Grimthorr\LaravelUserSettings\Setting;
use Illuminate\Support\Facades\Input;
use Session;
use File;


class SettingsController extends Controller
{
    /**afficher la page de parametre
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $default = json_decode(Storage::disk('local')->get('defaultSettings.json'), true)['0'];

        $settings = $this->getUserSettings();
        $user = User::getAuth();
        $categories = category::getAllOfUser();
        return view('frontend.app.settings', compact(['settings', 'categories', 'user', 'default']));
    }

    /**retourne la liste des parametre de l'utilisateur en ajax
     * @return mixed
     */
    public function getUserSettings()
    {
        $settings = User::getSettings();

        return json_decode($settings, true);
    }

    /**Mettre à jour le profil de l'utilisateur
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateProfil($id, Request $request)
    {
        if (User::updateModel($id, $request)) {
            return redirect()->back()->with('info', __('labels.frontend.user.profile.updated'));
        }
    }

    /**Mettre à jour le mot de passe de l'utilisateur
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updatePassword($id, Request $request)
    {
        if (User::updatePassword($id, $request)) {
            return redirect()->back()->with('info', __('labels.frontend.user.passwords.updated'));
        }
    }

    /**mettre a jour l'image de l'utilisateur
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function uploadUserImage($id, Request $request)
    {

        if ($request->hasFile('file')) {

            $this->removeDirectory(public_path('/images/users/' . $id . '/profil'));
            $userImage = $request->file('file');
            $imageName = 'ProfilImage.jpg';

            $userImage->move(public_path('/images/users/' . $id . '/profil'), $imageName);

            User::updateProfilImage($id,'/images/users/' . $id . '/profil/'.$imageName );

        }

        return response()->json(['status' => true, 'Message' => 'Image uploaded']);
    }

    /**Effacer  l'image de l'utilisateur
     * @return \Illuminate\Http\JsonResponse
     */
    public function deletePictureUser()
    {
        $id = Input::get('id');
        if (User::updateProfilImage($id, '')) {
            return response()->json(['status' => true, 'Message' => 'Image deleted']);
        }
    }

    function removeDirectory($path)
    {
        $files = glob(preg_replace('/(\*|\?|\[)/', '[$1]', $path) . '/{,.}*', GLOB_BRACE);
        foreach ($files as $file) {
            if ($file == $path . '/.' || $file == $path . '/..') {
                continue;
            } // skip special dir entries
            is_dir($file) ? removeDirectory($file) : unlink($file);
        }
        rmdir($path);
        return;
    }
}
