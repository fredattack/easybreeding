<?php

namespace App\Http\Controllers\Frontend\App;

use App\Bird;
use App\CustomSpecie;
use App\Order;
use App\Specie;
use App\Famille;
use App\BirdsAlpha;
use App\Cage;
use App\Picture;
use App\Models\Auth\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;


/**
 * Class HomeController.
 */
class BirdsController extends Controller
{


    /********************************************
     * Description: return view Bird.index with all birds
     * Parameters: NULL
     * Return view with compact params
     *********************************************/

    public function index()
    {
        $birds = Bird::getAllofUser();
        $data = $this->getRightSpecies($birds);
        $user = User::getAuth();

        $customSpecies = (array)Specie::getUsersSpecies();

        return view('frontend.app.bird.birdsIndex', compact(['data', 'customSpecies', 'user']));
    }


    /**Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {

        $query = Input::get('nbfc');

        $orders = Order::get();
        $females = Bird::where('sexe', '=', 'female')->get();
        $males = Bird::where('sexe', '=', 'male')->get();
        $user = User::getAuth();

        $customSpecies = (array)Specie::getUsersSpecies();

        $cages = Cage::getAllOfUser();
        return view('frontend.app.bird.birdsCreate', compact(['orders', 'females', 'males', 'customSpecies', 'query', 'cages', 'user']));
    }

    /**Attribuer une cage à un oiseau
     * @return \Illuminate\Http\JsonResponse
     */
    public function setBirdCage()
    {
        $birdId = Input::get('birdId');
        $cageId = Input::get('cageId');
        ((Input::get('supp')) ? $index = 0 : $index = $cageId);

        if (Bird::setBirdCage($birdId, $index)) {
            $bird = Bird::getModel($birdId);//
            return response()->json($bird);
        }

    }

    /**recupérer les données de la barre de recherche autoComplete
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajaxAutocompleteData()
    {
        $query = Input::get('query');
        $columnName = 'name_FR';
        $posts = BirdsAlpha::Where($columnName, 'LIKE', '%' . $query . '%')->get();
        return response()->json($posts);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
//        1 Gestion de l'espèce
        $specieId = '';
        switch ($request->type) {
            case 'specie':
                $specieId = $this->createCustomeSpecieId($request->speciesId);
                break;

            case 'userSpecie':
                $specieId = $request->customSpeciesId;
                break;

            case 'newSpecie':
                ///Create New Specie
                $specie = new CustomSpecie();
                $idUser = Auth::id();
                $specieId = CustomSpecie::where('idUser', '=', Auth::id())->count() . '_' . Auth::id();
                $specie->customId = $specieId;
                $specie->idUser = $idUser;

                $specie->commonName = $request->newCommonName;
                $specie->scientificName = $request->newScientificName;
                $specie->incubation = $request->incubation;
                $specie->fertilityControl = $request->fertilityControl;
                $specie->girdleDate = $request->girdleDate;
                $specie->outOfNest = $request->outOfNest;
                $specie->weaning = $request->weaning;
                $specie->sexualMaturity = $request->sexualMaturity;
                $specie->spawningInterval = $request->spawningInterval;
                if ($specie->save()) $response = 'Update Done';
                break;
        }
        
//        2 gestion de l'oiseau 
        $bird = new Bird;
        $bird->species_id = $specieId;
        $bird->sexe = $request->sexe;
        $bird->sexingMethode = $request->sexingMethode;
        $bird->origin = $request->origin;
        $bird->breederId = $request->breederId;
        $bird->cageId = $request->cageId;
        $bird->idType = $request->idType;
        $bird->idNum = $request->idNum;
        $bird->dateOfBirth = Carbon::createFromFormat('d/m/Y', $request->dateOfBirth);
        $bird->disponibility = $request->disponibility;
        $bird->status = $request->status;

        if ($request->mother_id != 1) $bird->mother_id = $request->mother_id;
        if ($request->father_id != 1) $bird->father_id = $request->father_id;
        if ($request->mutation != null) $bird->mutation = $request->mutation;

        if ($request->personal_id == null) {
            $count = Bird::where('species_id', '=', $specieId)
                ->where('userId', '=', Auth::id())->count();
            $count++;
            if ($request->commonName) $name = $request->commonName;
            else if ($request->newCommonName) $name = $request->newCommonName;
            else {
                if(str_contains($specieId, '_')) $name = CustomSpecie::where('customId', $specieId)->first()->commonName;
                else $name = Specie::where('customId', $specieId)->first()->commonName;
            }
            $bird->personal_id = str_replace(" ", "_", $name) . "_" . $count;
        } else $bird->personal_id = $request->personal_id;
        $bird->userId = Auth::id();

        $bird->save();
        if ($request->nbfc == true) return redirect(route('frontend.app.couples', ['nbfc' => $bird->id]));
        else {
            return redirect(route('frontend.app.birds'))->with('info', trans('alerts.frontend.birdCreated'));
        }
    }

    /** creer l'identifiant de la nouvelle espece
     * @param $id
     * @return string
     */
    public function createCustomeSpecieId($id)
    {
        $newid = $id . '_' . Auth::id();
        $count = CustomSpecie::where('customId', '=', $newid)->count();
        if ($count > 0) return $newid;
        else return $id;
    }

    /**
     * return les données de statistique du tableau de bord
     * @return \Illuminate\Http\JsonResponse
     */
    public function getStats()
    {
        $stats['status'] = $this->generateStatusStats(Bird::getAllofUser());
        $stats['eggs'] = app('App\Http\Controllers\Frontend\App\EggController')->generateEggsStats();
        $stats['laying'] = app('App\Http\Controllers\Frontend\App\NestlingController')->generateLayingStats();
        return response()->json($stats);
    }

    /** Modifie les infos de l'oiseau
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $orders = Order::get();
        $bird = Bird::where('id', '=', $id)->first();
        $idSpecie = $bird->species_id;

        if (!str_contains($idSpecie, '_')) {
            $specie = Specie::where('id', $idSpecie)->first();
            $famille = Famille::where('id', $specie->id_famillie)->first();
            $order = Order::where('id', $famille->orderId)->first();
        } else {
            $specie = CustomSpecie::where('customId', $idSpecie)->first();
            $idFamille = Specie::where('id', $specie->idReferedSpecies)->first()->id_famillie;
            Log::info('idFamille: ' . $idFamille);
            ($idFamille != null) ? $famille = Famille::where('id', $idFamille)->first() : $famille = null;
            ($famille != null) ? $order = Order::where('id', $famille->orderId)->first() : $order = null;
        }

        return view('frontend.app.bird.birdsEdit', compact(['bird', 'specie', 'order', 'orders', 'famille']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update(Request $request)
    {
        $bird = Bird::with('specie')->where('id', '=', $request->id)->first();

        $bird->sexe = $request->sexe;
        $bird->sexingMethode = $request->sexingMethode;
        $bird->dateOfBirth = $request->dateOfBirth;
        $bird->idType = $request->idType;
        $bird->idNum = $request->idNum;
        $bird->personal_id = $request->personal_id;
        $bird->origin = $request->origin;
        $bird->breederId = $request->breederId;
        $bird->disponibility = $request->disponibility;
        $bird->status = $request->status;

        $bird->save();

        return $request->id;
    }

    /**retour la liste de famille de l'ordre reçu en ajax
     * @return \Illuminate\Http\JsonResponse
     */
    public function generateFamillies()
    {
        $orderId = Input::get('orderId');
        $famillies = Famille::where('orderId', '=', $orderId)->get();

        return response()->json($famillies);
    }

    /**retour la liste d'espèce de la famille reçue en ajax
     * @return \Illuminate\Http\JsonResponse
     */
    public function generateSpecies()
    {
        $famillyId = Input::get('famillieId');
        $species = Specie::where('Id_famillie', '=', $famillyId)->get();

        return response()->json($species);
    }

    /**retourne le nom usuel de l'espéce reçue en ajax
     * @return \Illuminate\Http\JsonResponse
     */
    public function generateUsualName()
    {
        $specieId = Input::get('specieId');
        $selectedSpecie = Specie::where('id', '=', $specieId)->first();

        return response()->json($selectedSpecie);
    }

    /**retourn les infos de l'oiseau en ajax pour affichage dans la modal
     * @return \Illuminate\Http\JsonResponse
     */
    public function getBird()
    {
        $id = Input::get('id');
        $bird = Bird::where('id', '=', $id)->first();
        $pictures = Picture::where('birdId', '=', $id)->get();
        $specie = Specie::where('id', '=', $bird->species_id)->first();
        $data = [$bird, $specie, $pictures];
        return response()->json($data);
    }

    /**Retourn la liste des oiseaux de l'utilisateur en ajax
     * @return \Illuminate\Http\JsonResponse
     */
    public function getBirdsList()
    {
        $data = Bird::getAllofUser();
        return response()->json($data);
    }

    /********************************************
     * Description: return the specie or CustomSpecie of each Birds
     * Parameters: $birds a list of bird
     * Return $data
     *********************************************/
    public function getRightSpecies($birds)
    {
        $data = [];
        foreach ($birds as $bird) {
            $temp = [$bird];
            if (str_contains($bird->species_id, '_')) {
                $specie = CustomSpecie::getModelById($bird->species_id);

            } else {
                $specie = Specie::getModelById($bird->species_id);


            }
            array_push($temp, $specie);
            array_push($data, $temp);

        }
        return $data;
    }

    /**
     * @param $birds
     * @return $status
     */
    public function generateStatusStats($birds)
    {
        $status = [];
        $nbrCoupled = $birds->where('status', '=', 'coupled')->count();
        $status['coupled'] = $nbrCoupled;
        $nbrSingle = $birds->where('status', 'single')->count();
        $status['single'] = $nbrSingle;
        $nbrRest = $birds->where('status', 'rest')->count();
        $status['rest'] = $nbrRest;
        return $status;
    }

    /** efface l'image de l'oiseau en db
     * @return string
     */
    public function deletePictureBird()
    {
        $id = Input::get('id');
        if (Picture::destroy($id)) return 'job Done';
    }

    /** télécharger l'imaghe dans le dossier et créer l'enregistrement en db
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function uploadImage($id, Request $request)
    {

        $userImage = $request->file('file');

        $imageName = $userImage->getClientOriginalName().'.'.$userImage->getClientOriginalExtension();

        $userId = Auth::id();


        $path = $userImage->move(public_path('/images/users/' . $userId . '/birds/' . $id . '/'), $imageName);
        $idImage = Picture::createModel($id, $imageName);
        return response()->json(['status' => true, 'Message' => 'Image uploaded', 'name' => $imageName, 'imageId' => $idImage, 'userId' => Auth::id()]);
    }

}
