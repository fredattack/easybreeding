<?php

namespace App\Http\Controllers\Frontend\App;

use App\category;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Controller;
use JavaScript;
use Illuminate\Support\Facades\Auth;
use App\Bird;
use App\Egg;
use App\Nestling;
use App\Models\Auth\User;
use App\Googl;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Grimthorr\LaravelUserSettings\Setting;
use Illuminate\Support\Facades\Input;
use Session;


/**
 * Class HomeController.
 */
class HomeController extends Controller
{
    /**Connexion au calnedier de l'utisilsateur si  celui à accepté
     * @return \Illuminate\View\View
     */
    public function login(Googl $googl, User $user, Request $request)
    {
        $code = Input::get('code');
        Log::info('Code = ' . $code);
        $client = $googl->client();

        if ($code != null) {
            Log::info('dans login with Code');
            Log::info('Code = ' . $code);
            $client->authenticate($request->get('code'));
            $token = json_encode($client->getAccessToken(), true);

            $plus = new \Google_Service_Plus($client);

            $google_user = $plus->people->get('me');

            $id = $google_user['id'];

            $email = $google_user['emails'][0]['value'];
            $first_name = $google_user['name']['givenName'];
            $last_name = $google_user['name']['familyName'];


            \Setting::set('calendar.userToken', $token);;
            \Setting::save();


            $jsontoken = Session::get('userToken');
            Log::info('La session = ' . $jsontoken);

            return redirect()->route('frontend.app.settings');

        } else {
            Log::info('dans else login without Code');
            $auth_url = $client->createAuthUrl();
            Log::info('auth_url');
            Log::info($auth_url);
            return redirect($auth_url);
        }
    }


    /**retourner le tableau de bord et les infos
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {

        if (\Setting::get('calendar.id') != null) {
            app('App\Http\Controllers\Frontend\App\gCalendarController')->syncCalendar();
//            dd('ok');
        }

        $user = User::getAuth();

        $birds = Bird::getAllofUser();
        $eggs = Egg::getAllofUser();
        $nestlings = Nestling::getAllOfUser();
        $calendar = app('App\Http\Controllers\Frontend\App\TasksController')->generateCalendar();
        $categories = category::getAllOfUser();
        $default = json_decode(Storage::disk('local')->get('defaultSettings.json'), true)['0'];



        return view('frontend.app.dashboard', compact(['birds', 'eggs', 'nestlings', 'calendar', 'categories', 'default', 'user']));
    }

    /**Retourne la liste des couleurs des catégories, soit par defaut soit suivant les param du user
     * @param $catergories
     * @param $settings
     * @param $default
     * @return array
     */
    public function getCategoriesColors($catergories, $settings, $default)
    {

        $cat = [];
        foreach ($catergories as $category) {
            $newcat = [];
            $newcat['id'] = $category->id;
            $newcat['name'] = $category->name;

            if ($settings['category'][$category->name] == null) {
                if ($default['category'][$category->name] == null) {
                    $color = '#000';
                } else {
                    $color = $default['category'][$category->name];
                }
            } else $color = $settings['category'][$category->name];
            $newcat['color'] = $color;
            array_push($cat, $newcat);
        }
        return $cat;
    }

    /**Methode utilisée uniquement pour ajouter des parametres par defaut
     * @return array
     */
    public function store()
    {
        try {
            $settingsValue = Storage::disk('local')->exists('defaultSettings.json') ? json_decode(Storage::disk('local')->get('defaultSettings.json')) : [];

            $inputData['category'] = [
                'laying' => '#f05050',
                'nestling' => '#1976D2',
                'default' => '#FFB22B',
                'controlFecundity' => '#000',
            ];

            array_push($settingsValue, $inputData);

            Storage::disk('local')->put('defaultSettings.json', json_encode($settingsValue));

            return $inputData;

        } catch (Exception $e) {

            return ['error' => true, 'message' => $e->getMessage()];

        }
    }

}
