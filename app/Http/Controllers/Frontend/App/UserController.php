<?php

namespace App\Http\Controllers\Frontend\App;

use App\Http\Controllers\Controller;
use Grimthorr\LaravelUserSettings\Setting;
use Illuminate\Http\Request;

class UserController extends Controller 
{

    /**Sauvegarder un parametre utilisateur
     * @param $name
     * @param $value
     */
    public function saveSettings($name,$value)
    {
        \Setting::set($name, $value);
        \Setting::save();
        echo 'done!!';
    }


  
}

?>