<li class="nav-item dropdown">
                            @php($val=$user->created_at->timestamp%5)
    @if(Auth::check())
        <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <img src="@if($user->profilImage==null)
            {{asset('/images/default/profilImages/'.$val.'.jpg')}}
            @else {{asset('/images/users/'.$user->id.'/profil/ProfilImage.jpg')}}
            @endif" alt="user" class="profile-pic" /></a>
        <div class="dropdown-menu dropdown-menu-right scale-up">
            <ul class="dropdown-user">
                <li>
                    <div class="dw-user-box">
                        <div class="u-img">

                            <img src="@if($user->profilImage==null)
                            {{asset('/images/default/profilImages/'.$val.'.jpg')}}
                            @else {{asset('/images/users/'.$user->id.'/profil/ProfilImage.jpg')}}
                            @endif" alt="user">

                        </div>
                        <div class="u-text">
                            <h4>{{$user->first_name}} {{$user->last_name}}</h4>
                            <p class="text-muted">{{$user->email}}</p><a href="{{route('frontend.app.settings',['target' => 'viewProfile'])}}" class="btn btn-rounded btn-danger btn-sm">View Profile</a></div>
                    </div>
                </li>

                <li role="separator" class="divider"></li>
                @role('administrator')

                    <li><a href="{{route('admin.dashboard')}}"><i class="ti-settings"></i> Paneau d'administration</a></li>
                    <li role="separator" class="divider"></li>


                @endrole

                {{--<li><a href="#"><i class="ti-user"></i> My Profile</a></li>--}}
                {{--<li><a href="#"><i class="ti-wallet"></i> My Balance</a></li>--}}
                {{--<li><a href="#"><i class="ti-email"></i> Inbox</a></li>--}}
                {{--<li role="separator" class="divider"></li>--}}
                <li><a href="{{route('frontend.app.settings',['target' => 'viewCal'])}}"><i class="ti-settings"></i> Votre Profil</a></li>
                <li role="separator" class="divider"></li>
                <li><a href="{{route('frontend.auth.logout')}}"><i class="fa fa-power-off"></i> @lang('navs.general.logout')</a></li>
            </ul>
        </div>
    @else
        <li class="nav-item">
            <a class="nav-link text-muted text-muted waves-effect waves-dark"  href="{{route('frontend.auth.login')}}"><i class="mdi mdi-account"></i>login </a>
            <a class="nav-link text-muted text-muted waves-effect waves-dark" href="{{route('frontend.auth.register')}}"><i class="mdi mdi-account-plus"></i> register </a>
        </li>

    @endif



</li>