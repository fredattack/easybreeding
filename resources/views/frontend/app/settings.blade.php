@extends('frontend.layouts.customApp')

@section('title', app_name() . ' | '.__('custom.settings'))

@section('content')
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">{{__('custom.settings')}}</h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item active"><a
                            href="{{route('frontend.app.settings')}}">{{__('custom.settings')}}</a></li>


            </ol>
        </div>
        <div>
            {{--<button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>--}}
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid settingsPage">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        @if (session('info'))
            <div class="alert alert-success">
                {{ session('info') }}
            </div>
        @endif
        <div class="row">
            <div id="accordion">
                <!-- ============================================================== -->
                <!-- User  -->
                <!-- ============================================================== -->
                <div class="card" id="userProfil">
                    <div class="card-header" id="headingUser">
                        <h5 class="mb-0">
                            <button class="btn btn-table" data-toggle="collapse" data-target="#collapseUser"
                                    aria-expanded="false" aria-controls="collapseOne">
                                <i class="fa fa-plus"></i> @lang('labels.frontend.settings.userProfil')
                            </button>
                        </h5>
                    </div>

                    <div id="collapseUser" class="collapse " aria-labelledby="headingUser" data-parent="#accordion">
                        <div class="card-body">
                            <div class="row infoLine buttonRow">
                                <div class="col-md-11">

                                </div>
                                <div class="col-md-1 pull-right">
                                    <span>
                                       <a href="" class="btn btn-lg btn-circle btn-secondary" data-toggle="tooltip"
                                          title="{{__('alerts.frontend.editProfile')}}" data-placement="bottom"
                                          id="editProfilBtn">
                                         <i id="btnIcon" class="fa fa-pencil"></i>
                                      </a>
                                   </span>

                                </div>
                            </div>
                            <div class="row">
                                <!-- Column -->
                                <div class="col-lg-6 col-md-6 col-sm-12">
                                    <center class="m-t-10">
                                        @php($val=$user->created_at->timestamp%5)
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="card dropCard">
                                                    <div class="card-body">
                                                        <form id="myDropzone" method="post"
                                                              action="{{route('frontend.app.uploadUserImage',$user->id)}}"
                                                              class="dropzone"></form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        {{--@php(dd((asset('/images/users/'.$user->id.'/profil/ProfilImage.jpg'))))--}}
                                        <img src="
                                                @if($user->profilImage==null)
                                        {{asset('/images/default/profilImages/'.$val.'.jpg')}}
                                        @else {{asset('/images/users/'.$user->id.'/profil/ProfilImage.jpg')}}
                                        @endif" class="img-circle imageZone" width="250"/>

                                        <div class="row m-t-10 col-sm-12">

                                            <div class="col-sm-6 col-lg-6  text-right ">
                                                @if($user->profilImage!=null)
                                                    <button class="btn btn-circle btn-secondary editPictureBtn"
                                                            id="deletePictureBtn" value="{{$user->id}}"><i
                                                                class="fa fa-trash"></i></button>
                                                @endif
                                            </div>
                                            <div class="col-sm-6 col-lg-6 text-left">
                                                <button class="btn btn-circle btn-secondary  editPictureBtn"
                                                        id="editPictureBtn">
                                                    <i id="editPictureIcon" class="fa fa-pencil"></i></button>
                                            </div>
                                        </div>


                                        {!! Form::open(array('route' => ['frontend.app.updateProfil',$user->id], 'method' => 'POST','id'=>'UpdateProfilForm','onkeypress'=>"return event.keyCode != 13","data-parsley-validate"=>"")) !!}

                                        <h2 class="card-title m-t-10 profilText">{{$user->first_name}} {{$user->last_name}}</h2>
                                        <div class="row m-t-10 col-sm-12">
                                            <div class="col-sm-6 col-lg-6  text-right ">
                                                <input type="text" class="form-control profilInput"
                                                       value="{{$user->first_name}}" placeholder="John"
                                                       name="first_name" id="first_nameInput" required>
                                            </div>
                                            <div class="col-sm-6 col-lg-6 text-left">
                                                <input type="text" class="form-control profilInput"
                                                       value="{{$user->last_name}}" placeholder="Doe" name="last_name"
                                                       id="last_nameInput" required>
                                            </div>
                                        </div>
                                        {{--<h6 class="card-subtitle m-t-10">Accoubts Manager Amix corp</h6>--}}
                                        {{--<div class="row text-center justify-content-md-center">--}}
                                        {{--<div class="col-4"><a href="javascript:void(0)" class="link"><i class="icon-people"></i> <font class="font-medium">254</font></a></div>--}}
                                        {{--<div class="col-4"><a href="javascript:void(0)" class="link"><i class="icon-picture"></i> <font class="font-medium">54</font></a></div>--}}
                                        {{--</div>--}}

                                    </center>


                                </div>

                                <div class="col-lg-6 col-md-6 col-sm-12 form-group">
                                    <small class="text-muted p-t-10 db">Email address</small>
                                    <span>
                                        <button class="btn btn-circle btn-secondary"><i
                                                    class="fa fa-envelope"></i></button>
                                        <h6 class="profilText">{{$user->email}}</h6>
                                         <input type="text" class="form-control profilInput" value="{{$user->email}}"
                                                placeholder="JohnDoe@mail.com" name="email" id="emailInput" required>
                                    </span>
                                    <small class="text-muted p-t-16 db">Phone</small>
                                    <span>
                                        <button class="btn btn-circle btn-secondary"><i
                                                    class="fa fa-phone"></i></button>
                                        <h6 class="profilText">{{$user->phone}}</h6>
                                         <input type="text" class="form-control profilInput" value="{{$user->phone}}"
                                                placeholder="0456/12 34 56" name="phone" id="phoneInput"
                                                data-parsley-pattern="^\d{4} \d{2} \d{2} \d{2}$" required>
                                    </span>
                                    <small class="text-muted p-t-16 db">Address</small>
                                    <span>
                                        <button class="btn btn-circle btn-secondary"><i class="fa fa-home"></i></button>
                                        <h6 class="profilText">{{$user->adress}}</h6>
                                         <input type="text" class="form-control profilInput" value="{{$user->adress}}"
                                                placeholder="" name="adress" id="adressInput">
                                    </span>
                                    {{--<div class="map-box">--}}
                                    {{--<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d470029.1604841957!2d72.29955005258641!3d23.019996818380896!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x395e848aba5bd449%3A0x4fcedd11614f6516!2sAhmedabad%2C+Gujarat!5e0!3m2!1sen!2sin!4v1493204785508" width="100%" height="150" frameborder="0" style="border:0" allowfullscreen></iframe>--}}
                                    {{--</div> --}}
                                    {{--<br/>--}}

                                    <small class="text-muted p-t-30 db">Social Profile</small>
                                    <div>
                                        <span>
                                        <a href="{{$user->facebook}}" class="btn btn-circle btn-secondary"><i
                                                    class="fa fa-facebook"></i></a>
                                        <h6 class="profilText">{{$user->facebook}}</h6>
                                         <input type="text" class="form-control profilInput" value="{{$user->facebook}}"
                                                placeholder="" name="facebook" id="facebookInput">
                                        </span>
                                        <span>
                                        <a href="{{$user->twitter}}" class="btn btn-circle btn-secondary"><i
                                                    class="fa fa-twitter"></i></a>
                                        <h6 class="profilText">{{$user->twitter}}</h6>
                                         <input type="text" class="form-control profilInput" value="{{$user->twitter}}"
                                                placeholder="" name="twitter" id="twitterInput">
                                        </span>
                                        <span>
                                            <a href="{{$user->instagram}}" class="btn btn-circle btn-secondary"><i
                                                        class="fa fa-instagram"></i></a>
                                            <h6 class="profilText">{{$user->instagram}}</h6>
                                            <input type="text" class="form-control profilInput"
                                                   value="{{$user->instagram}}" placeholder="" name="instagram"
                                                   id="instagramInput">
                                        </span>


                                        <div class="row infoLine">

                                            <div class="form-group">
                                                <button id="updateProfilBtn" type="submit"
                                                        class="btn btn-lg btn-secondary text-center">@lang('labels.general.submit')</button>
                                            </div>
                                        </div>
                                        {!! Form::close() !!}

                                        <div class="row infoLine">
                                            <a id="to-recover" class="btn btn-outline-danger pull-left m-t-10"><i
                                                        class="fa fa-lock m-r-5"></i>{{ __('labels.frontend.passwords.reset_password_button') }}
                                            </a>
                                        </div>

                                        <form class="form-horizontal p-t-30 mx-auto" id="recoverpasswd" method="post"
                                              action="{{route('frontend.app.updatePassword',$user->id) }}"
                                              onkeypress="return event.keyCode != 13" data-parsley-validate="">

                                            <div class="form-group  ">
                                                <div class="col-xs-12">
                                                    <input id="password1" name="password" type="password"
                                                           class="form-control"
                                                           placeholder="@lang('validation.attributes.frontend.password')"
                                                           data-parsley-minlength="8"
                                                           data-parsley-errors-container=".errorspannewpassinput"
                                                           data-parsley-required/>
                                                    <span class="errorspannewpassinput"></span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-xs-12">
                                                    <input name="Password_2" id="password2" type="password"
                                                           class="form-control"
                                                           placeholder="@lang('validation.attributes.frontend.password')"
                                                           data-parsley-errors-container=".errorspanconfirmnewpassinput"
                                                           data-parsley-equalto="#password1"
                                                           data-parsley-required/>
                                                    <span class="errorspanconfirmnewpassinput"></span>
                                                </div>
                                            </div>
                                            <div class="form-group text-center m-t-20">
                                                <div class="col-xs-12">
                                                    <button class="btn btn-secondary btn-lg text-uppercase waves-effect waves-light"
                                                            type="submit">Reset
                                                    </button>
                                                </div>
                                            </div>
                                        </form>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- Display  -->
                <!-- ============================================================== -->
            {{--<div class="card">--}}
            {{--<div class="card-header" id="headingGeneral">--}}
            {{--<h5 class="mb-0">--}}
            {{--<button class="btn btn-table collapsed" data-toggle="collapse" data-target="#collapseGeneral" aria-expanded="false" aria-controls="collapseTwo">--}}
            {{--<i class="fa fa-plus"></i> @lang('labels.frontend.settings.general')--}}
            {{--</button>--}}
            {{--</h5>--}}
            {{--</div>--}}
            {{--<div id="collapseGeneral" class="collapse" aria-labelledby="headingGeneral" data-parent="#accordion">--}}
            {{--<div class="card-body">--}}
            {{--Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--</div>--}}
            <!-- ============================================================== -->
                <!-- Calendar  -->
                <!-- ============================================================== -->
                <div class="card">
                    <div class="card-header" id="headingCalendar">
                        <h5 class="mb-0">
                            <button class="btn btn-table collapsed" data-toggle="collapse"
                                    data-target="#collapseCalendar" aria-expanded="true" aria-controls="collapseThree">
                                <i class="fa fa-plus"></i> @lang('labels.frontend.settings.calendar')
                            </button>
                        </h5>
                    </div>
                    <div id="collapseCalendar" class="collapse" aria-labelledby="headingCalendar"
                         data-parent="#accordion">
                        <div class="card-body">
                            <div class="form-group row">
                                <label class="control-label text-right col-md-3">@lang('labels.frontend.calendar.googleAccount')
                                    <sup><i class="fas fa-question-circle" data-toggle="tooltip"
                                            title="{{__('labels.frontend.calendar.googleInfo')}}"
                                            data-placement="top"></i></sup></label>

                                <div class="col-md-3">
                                    <div class="col-md-3">
                                        <a href="{{route('loginGoogle')}}"
                                           class="btn btn-danger">@lang('labels.frontend.auth.signIn')</a>
                                    </div>
                                </div>
                                {{--<div class="col-md-2">--}}
                                    {{--<div class="col-md-3">--}}
                                        {{--<a href="{{route('CreateCalendar')}}"--}}
                                           {{--class="btn btn-primary">CreateCalendar</a>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="col-md-2">--}}
                                    {{--<div class="col-md-3">--}}
                                        {{--<a href="{{route('addAllEvent')}}"--}}
                                           {{--class="btn btn-warning">addAllEvent</a>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="col-md-2">--}}
                                    {{--<div class="col-md-3">--}}
                                        {{--<a href="{{route('deleteAllEvent')}}"--}}
                                           {{--class="btn btn-success">deleteAllEvent</a>--}}
                                    {{--</div>--}}
                                {{--</div>--}}


                            </div>
                            <div class="form-group row">
                                <label class="control-label text-right col-md-3">@lang('labels.frontend.calendar.categories')</label>
                                <div class="col-md-9 catgroup">
                                    @foreach($categories as $category)
                                        <div class="row" id="row{{$category->id}}">
                                            <label class="control-label col-md-4">{{($category->userId==0)?__('labels.frontend.calendar.'.$category->name):$category->name}}</label>
                                            <input class="col-md-8 categoryColorInput" type='text'
                                                   id="full{{$category->id}}" data-type="{{$category->userId}}"/>
                                            @if($category->userId!=0)<a class="col-md-1 btn btn-table deleteCat"
                                                                        id="btnD{{$category->id}}"><i
                                                        class="fa fa-times"></i>
                                            </a>@endif
                                        </div>
                                        @endforeach
                                        </br>
                                        <div class="row">
                                            <label class="control-label col-md-4"
                                                   id="categoryLabel"><strong>@lang("labels.frontend.calendar.AddNewCategory")</strong></label>
                                            {{--<span class="col-md-3"></span>--}}
                                            <a id="addCategoryBtn2" class=" btn btn-table ">
                                                <i id="addCategoryIcon"
                                                   class="fa fa-plus"></i>
                                            </a>
                                        </div>
                                        <div class="row">
                                            <div class="input-group addCatGroup col-md-8" id="addGroupCat">
                                                <input type="text" class="form-control "
                                                       placeholder="@lang("labels.frontend.calendar.name")"
                                                       id="newCategoryInput">
                                                <small class="form-control-feedback"> This is inline help</small>
                                                <div class="input-group-append " data-toggle="tooltip"
                                                     title="{{__('alerts.frontend.colorCategory')}}"
                                                     data-placement="top">
                                                    <input type='text' id="full"/>
                                                </div>
                                            </div>
                                            <button type="button"
                                                    class="btn btn-success btn-circle btn-lg addCatGroup categoryGroup"
                                                    id="saveCategoryset" data-toggle="tooltip"
                                                    title="{{__('alerts.frontend.saveCategory')}}"
                                                    data-placement="bottom"><i class="fa fa-plus"></i></button>
                                        </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="control-label text-right col-md-3">@lang('labels.frontend.calendar.defaultView')</label>
                                <div class="col-md-8 pull-right">
                                    <div class="radio-list text-center">
                                        <label class="custom-control custom-radio">
                                            <input id="calendarDefaultView1" name="defaultView" type="radio"
                                                   value='agendaDay' class="custom-control-input"
                                                   @if(isset($settings['defaultView']))
                                                   @if ($settings['defaultView']=='dayView')
                                                   checked
                                                    @endif
                                                    @endif>
                                            <span class="custom-control-indicator"></span>
                                            <span class="custom-control-description">@lang('labels.frontend.date.day')</span>
                                        </label>
                                        <label class="custom-control custom-radio">
                                            <input id="calendarDefaultView2" name="defaultView" type="radio"
                                                   value="listView" class="custom-control-input"
                                                   @if(isset($settings['defaultView']))
                                                   @if($settings['defaultView']=='list')
                                                   checked
                                                   @endif
                                                   @elseif(!isset($settings['defaultView']))
                                                   checked
                                                    @endIf>
                                            <span class="custom-control-indicator"></span>
                                            <span class="custom-control-description">@lang('labels.frontend.date.list')</span>
                                        </label>
                                        <label class="custom-control custom-radio">
                                            <input id="calendarDefaultView3" name="defaultView" type="radio"
                                                   value="month" class="custom-control-input"
                                                   @if(isset($settings['defaultView']))
                                                   @if ($settings['defaultView']=='month')
                                                   checked
                                                    @endIf
                                                    @endif>
                                            <span class="custom-control-indicator"></span>
                                            <span class="custom-control-description">@lang('labels.frontend.date.month')</span>
                                        </label>
                                        <label class="custom-control custom-radio">
                                            <input id="calendarDefaultView4" name="defaultView" type="radio"
                                                   value="agendaWeek" class="custom-control-input"
                                                   @if(isset($settings['defaultView']))
                                                   @if ($settings['defaultView']=='agendaWeek')
                                                   checked
                                                    @endIf
                                                    @endif>
                                            <span class="custom-control-indicator"></span>
                                            <span class="custom-control-description">@lang('labels.frontend.date.week')</span>
                                        </label>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <!-- ============================================================== -->

        </div>
        <!-- ============================================================== -->
        <!-- End Container fluid  -->
        <!-- ============================================================== -->
        @endsection
        @section('script')

            {{--<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script>--}}
            <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/dropzone.js"></script>
            <script type="text/javascript" src="{{mix('/js/appfrontendDatable.js')}}"></script>
            @foreach($categories as $category)
                <?php
                if (isset($settings['category'][$category->name])) {
                    $color = $settings['category'][$category->name];

                } else {
                    if (isset($default['category'][$category->name])) {
                        $color = $default['category'][$category->name];
                    } else {
                        $color = '#000';
                    }
                }
                ?>
                <script>
                    $("#full{{$category->id}}").spectrum({
                        color: '{{$color}}',
                    });
                </script>


            @endforeach
    </div>

@endsection