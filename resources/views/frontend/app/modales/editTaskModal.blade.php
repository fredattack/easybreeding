<!-- The Modal -->


    <div class="modal  fade" id="editTaskModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" >
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                        <h2 id="headerEdit"></h2>
                        <button type="button" class="btn btn-circle btn-xl btn-table m-r-30" value="" data-toggle="tooltip" title="{{__('alerts.frontend.deleteEvent')}}" data-placement="bottom" id="btnDeleteEventModal">
                            <i class="fa fa-trash-o"></i>
                       </button>
                    <button type="button" class="btn btn-circle btn-xl btn-table" data-toggle="tooltip" title="{{__('alerts.frontend.close')}}" data-placement="bottom" id="btnCloseEditEventModal">
                            <i class="mdi mdi-close"></i>
                       </button>

                </div>

                <div class="modal-body">

                   {!! Form::open(array('route' => 'frontend.app.editTask',
                                        'method' => 'POST',
                                        'id'=>'editTask',
                                        'onkeypress'=>"return event.keyCode != 13",
                                        "data-parsley-validate"=>"")) !!}

                    <input type="hidden" id="id" name="id">

                    <label class="control-label">@lang("labels.frontend.calendar.title")</label>

                    <input class="form-control form-white" placeholder="@lang("labels.frontend.calendar.addTitle")" type="text" name="name" id="nameEdit" required/>
                    <p>@lang('alerts.frontend.from')</p>
                    <div class="input-group">
                        <input type="text" class="form-control startDate" id="startDateEdit" name="startDate" placeholder="@lang("labels.frontend.calendar.mm/dd/yyyy")" >
                        <div class="input-group-append">
                            <span class="input-group-text"><i class="icon-calender"></i></span>
                        </div>
                    </div>
                    <div class="input-group timeEventEdit" data-placement="bottom" data-align="top" data-autoclose="true" >
                        <input type="text" class="form-control" name="startTime" id="startTimeEdit"  placeholder="08:00">
                        <div class="input-group-append">
                            <span class="input-group-text"><i class="fa fa-clock-o"></i></span>
                        </div>
                    </div>
                    <p>@lang('alerts.frontend.to')</p>
                    <div class="input-group">
                        <input type="text" class="form-control endDate" name="endDate" id="endDateEdit" placeholder="@lang("labels.frontend.calendar.mm/dd/yyyy")" >
                        <div class="input-group-append">
                            <span class="input-group-text"><i class="icon-calender"></i></span>
                        </div>
                    </div>
                    <div class="input-group  timeEventEdit "  data-placement="bottom" data-align="top" data-autoclose="true" >
                        <input type="text" class="form-control" name="endTime" id="endTimeEdit" placeholder="08:00" >
                        <div class="input-group-append">
                            <span class="input-group-text"><i class="fa fa-clock-o"></i></span>
                        </div>
                    </div>
                    {{--<div class="col-md-12">--}}
                    <input type="checkbox"  name="allDay" id="allDayEdit"/>
                    <label for="allDay_checkbox">@lang("labels.frontend.calendar.allDay")</label>

                    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" />
                    <select class="form-control form-white categoryGroup" id="categorySelectEdit" name="categoryId" required>
                        <option value="" selected disabled >@lang("labels.frontend.calendar.chooseCategory")</option>
                        @foreach($categories as $category)
                            <option value="{{$category->id}}" {{--style="color:{{($default['category']==null)?$settings['category'][$category->name]:$default['category'][$category->name]}}"--}}>{{$category->name}}</option>
                        @endforeach
                    </select>

                </div>

                <div class="modal-footer">
                    <div class="row " id="footerRow">

                        <button type="submit" id='submitCouple' class="btn btn-lg btn-success">@lang('labels.general.submit')</button>
                    </div>

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>


