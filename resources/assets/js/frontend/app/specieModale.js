
let specieId=0;

jQuery(document).ready(function($) {

    $('#specieModal').on('show.bs.modal', function (event) {
        let button = $(event.relatedTarget);// Button that triggered the modal
        specieId = button.val(); // Extract info from data-* attributes
        getSpecie(specieId);
    })

});


function getSpecie(id) {
    $.get('/ajax/getSpecie?id='+id,function(data) {
        console.log('data',data);
        setText(data);
        setInputValue(data['2']);
        setImages(data);
    });
}

function setImages(data) {
    let image =data['3'];
    let specie =data['2'];

    console.log('setimages');
    console.log(data);

    if(image!=null){
            $('.leftSpecie').empty().append('<img class="d-block img-fluid" src="/images/users/'+data['4']+'/species/'+specie['customId']+'/'+image["imageUrl"]+'" alt="First slide">' );
    }
    else{

        $('.leftSpecie').empty().append('<img class="d-block img-fluid" src="/images/default/nophoto.jpg" alt="First slide">');
    }

    $("#innerSlide .carousel-item:first").addClass("active");
    $('#indexCarousel').carousel();

}





function setText(data) {

    console.log(data);
    let specie = data['2'];
    (data['0'] !=null ?  $('#orderText').text(data['0']) : "no ID pass");
    (data['1'] !=null ?  $('#famillyText').text(data['1']) : "");
    $('#NameText').text((specie['commonName']!=null ?  defineName(specie['commonName']) : Lang.get('labels.frontend.birds.unknow')));
    $('#scienceText').text((specie['scientificName']!=null ?  specie['scientificName'] : Lang.get('labels.frontend.birds.unknow')));
    $('#incubationText').text((specie['incubation']!=null ?  specie['incubation']+' '+Lang.get('labels.frontend.date.days') : Lang.get('labels.frontend.birds.unknow')));
    $('#fertilityControlText').text((specie['fertilityControl']!=null ?  specie['fertilityControl']+' '+Lang.get('labels.frontend.date.days') : Lang.get('labels.frontend.birds.unknow')));
    $('#spawningIntervalText').text((specie['spawningInterval']!=null ?  specie['spawningInterval']+' '+Lang.get('labels.frontend.date.days') : Lang.get('labels.frontend.birds.unknow')));
    $('#outOfNestText').text((specie['outOfNest']!=null ?  specie['outOfNest']+' '+Lang.get('labels.frontend.date.days') : Lang.get('labels.frontend.birds.unknow')));
    $('#weaningText').text((specie['weaning']!=null ?  specie['weaning']+' '+Lang.get('labels.frontend.date.days') : Lang.get('labels.frontend.birds.unknow')));
    $('#sexMatText').text((specie['sexualMaturity']!=null ?  specie['sexualMaturity']+' '+Lang.get('labels.frontend.date.days') : Lang.get('labels.frontend.birds.unknow')));
    $('#girdleDateText').text((specie['girdleDate']!=null ?  specie['girdleDate']+' '+Lang.get('labels.frontend.date.days') : Lang.get('labels.frontend.birds.unknow')));

}

function defineName(text){
   if (text.substring(0, 4) === 'name') {
       return Lang.get('species.'+text)
   }
   else return text;
}

function setInputValue(data) {

    let specie = data;


    $('#usualNameInput').val((specie['commonName']!=null ?  specie['commonName'] : ''));
    $('#scientificNameInput').val((specie['scientificName']!=null ?  specie['scientificName'] : ''));
    $('#incubationInput').val((specie['incubation']!=null ?  specie['incubation'] : ''));
    $('#fertilityControlInput').val((specie['fertilityControl']!=null ?  specie['fertilityControl'] : ''));
    $('#spawningIntervalInput').val((specie['spawningInterval']!=null ?  specie['spawningInterval'] : ''));
    $('#outOfNestInput').val((specie['outOfNest']!=null ?  specie['outOfNest'] : ''));
    $('#weaningInput').val((specie['weaning']!=null ?  specie['weaning'] : ''));
    $('#sexualMaturityInput').val((specie['sexualMaturity']!=null ?  specie['sexualMaturity'] : ''));
    $('#girdleDateInput').val((specie['girdleDate']!=null ?  specie['girdleDate'] : ''));
    $('#idInputSpecie').val(specie['customId']);

    $('#id_famillieInput').val(specie['id_famillie']);


}


$('#editCustomSpecieBtn').on('click',function (e) {
    e.preventDefault();
   switchView();
});

function switchView() {
    $("#specieModal input").show();
    $('#updateSpecieBtn').show();
    $('.modalText').css('display','none');
    $('#editCustomSpecieBtn').hide();
    $('#returnBackBtn').css('display','inline-block');
}

$( "#UpdateSpecieForm" ).submit(function(e) {
    e.preventDefault();
    let data =$( this ).serializeArray() ;

    setSpecie(data);
});


function setSpecie(data) {
    $.post( "/ajax/setSpecie",data).done(function( res ) {

        location.reload(true);
    });
}

function unSwitchViewSpecie(){
    $("#specieModal input").hide();
    $('#updateSpecieBtn').hide();
    $('.modalText').css('display','block');

    $('#editCustomSpecieBtn').css('display','inline-block');
    $('#returnBackBtn').hide();
}


$('#returnBackBtn').on('click',function () {
    unSwitchViewSpecie();
});


$('#specieModal').on('hidden.bs.modal', function (e) {
   unSwitchViewSpecie();
});


/*Pictures*/

$('#editPicturesSpeciesBtn').on('click',function (e) {
    $('#editPictureSpeciesIcon').toggleClass('fa-pencil fa-rotate-left');
    // $('#innerSlide').fadeToggle();
    $('.editImageSpecies').fadeToggle();
});



/********************
 *
 editImage
 *
 * *************************/


Dropzone.options.speciePictureDropzone = {
    init: function() {
        console.log(specieId);
        this.on("processing", function(file) {
            this.options.url = "/app/uploadSpecieImage/"+specieId;
        });
        this.on("success", function(file, response) {
            console.log(file);
            $('.leftSpecie').empty();
            $('.leftSpecie').append('<img class="d-block img-fluid" src="/images/users/'+response['userId']+'/species/'+specieId+'/'+response['name']+'" alt="Specie image">');
            this.removeAllFiles();
            $('#item00').remove();
            $('#editPictureSpeciesIcon').toggleClass('fa-pencil fa-rotate-left');
            $('.editImageSpecies').fadeToggle();


        });
    },
    dictDefaultMessage:Lang.get("labels.frontend.settings.dropZoneDefaultMessage"),
    maxFilesize: 5,
    renameFile: function(file){
        var dt = new Date();
        var time = dt.getTime();
        return "birdImage_"+time+"."+file.name.split('.')[0];
    },
    acceptedFiles: ".jpeg,.jpg,.png,.gif",
    addRemoveLinks: true,
};
