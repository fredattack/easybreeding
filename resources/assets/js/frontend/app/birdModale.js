let birdId;
jQuery(document).ready(function($) {
    console.log('birdmodal');

    $('#birdModal').on('show.bs.modal', function (event) {
        let button = $(event.relatedTarget);// Button that triggered the modal
        let birdId = button.val(); // Extract info from data-* attributes
        getBird(birdId);
    })

});

function getBird(id) {
    $.get('/ajax/getBird?id='+id,function (data) {
        console.log(data);
        setText(data);
        setImages(data['2'],data['0']['userId']);
        setInputValue(data['0']);
    });
    birdId=id;

}

function setText(data) {
    let bird= data['0'];
    let specie = data['1'];


    $('#idPersoText').text(bird['personal_id']);
    $('#usualNameText').text(defineName(specie['commonName']));
    $('#dateOfBirthText').text(bird['dateOfBirth']);
    $('#speciesText').text(specie['scientificName']);
    $('#genderText').text(Lang.get('labels.frontend.birds.'+bird['sexe']));
    $('#sexingMethodeText').text(Lang.get('labels.frontend.birds.'+bird['sexingMethode']));
    $('#idNumText').text(bird['idNum']);
    $('#idTypeText').text(Lang.get('labels.frontend.birds.'+bird['idType']));
    $('#originText').text(Lang.get('labels.frontend.birds.'+bird['origin']));
    $('#breederText').text(bird['breederId']);
    $('#disponibilityText').text(Lang.get('labels.frontend.birds.'+bird['disponibility']));
    $('#statusText').text(Lang.get('labels.frontend.birds.'+bird['status']));

    displayItems(bird);
}

function defineName(text){
    if (text.substring(0, 4) === 'name') {
        return Lang.get('species.'+text)
    }
    else return text;
}

function setImages(data,userId) {
    setSlide(data,userId);
    $('.thumbnailsGroup').empty();
    $.each(data,function (index,picture) {
        $('.thumbnailsGroup').append('<div class="img-wrap">'+
                                        '<a class="close deleteImage" data-toggle="tooltip" title="'+Lang.get("alerts.frontend.editBird")+'"  data-placement="bottom" id="delbtn'+picture['id']+'">'+
                                            '<i class="fa fa-times-circle-o"></i>'+
                                        '</a>'+
                                        '<img id="img'+picture['id']+'" class="d-block img-fluid" src="/images/users/'+userId+'/birds/'+birdId+'/'+picture["imageUrl"]+'" alt="First Image">'+
                                    '</div>');
    });

}

function setSlide(data,userId) {

    $('#innerSlide').empty();

    if(data.length >0){
            $.each(data,function (index,picture) {

                    $('#innerSlide').append('<div class="carousel-item " id="item'+picture["id"]+'">\n' +
                        '                                    <img class="d-block img-fluid" src="/images/users/'+userId+'/birds/'+birdId+'/'+picture["imageUrl"]+'" alt="First slide">' +
                        '                                </div>');

            });
    }
    else{

        $('#innerSlide').append('<div class="carousel-item " id="item00">' +
            '                                    <img class="d-block img-fluid" src="/images/default/nophoto.jpg" alt="First slide">' +
            '                                </div>');
    }

    $("#innerSlide .carousel-item:first").addClass("active");
    $('#indexCarousel').carousel();

}

$('.thumbnailsGroup').on('click', '.deleteImage', function (event) {
    // Do something
    console.log('click deleteImage');
    let btnId = $(this).attr('id');
   let id = btnId.substr(6);
    $.get('/ajax/deletePictureBird?id=' + id , function (data) {
        console.log('return deleteImage');
        console.log('#img' + id);
        $('#img' + id).css('visibility','hidden');
        $('#' + btnId).css('visibility','hidden');
        $('#item' + id).remove();
        // location.reload();
    });
});



function displayItems(bird) {
    if(!bird['breederId'] || !bird['origin']=='thisElevage')$('#breederGroup').css('display','none');
    bird['sexe'] != 'unknow' ?  $('#genderGroup').css('display','block') : $('#genderGroup').css('display','none');
    bird['idType'] != 'noOne' ?  $('#idNummerGroup').css('display','block') : $('#genderGroup').css('display','none');


}

function setInputValue(data) {

    let bird = data;
     console.log(bird);
    $('#personal_idInput').val((bird['personal_id']!=null ?  bird['personal_id'] : ''));
    $('#dateOfBirthInput').val((bird['dateOfBirth']!=null ?  bird['dateOfBirth'] : ''));
    $('#sexingMethodeSelect').val(bird['sexingMethode']).prop('selected',true);
    $('#'+bird["sexe"]).prop('checked',true);
    $('#'+bird["idType"]).prop('checked',true);
    $('#idNumInput').val((bird['idNum']!=null ?  bird['idNum'] : ''));
    $('#originSelect').val(bird['origin']).prop('selected',true);
    $('#breederIdInput').val((bird['breederId']!=null ?  bird['breederId'] : ''));
    $('#'+bird["disponibility"]).prop('checked',true);
    $('#'+bird["status"]).prop('checked',true);
    $('#idInput').val(bird['id']);

}

$('#editBirdBtn').on('click',function (e) {
    e.preventDefault();
   switchView();
});

function switchView() {
    $("#birdModal input").show();
    $("#birdModal select").show();
    $("#birdModal .radio-list").css('display','grid');
    $('#returnBirdBtn').css('display','grid');
    $('#updateBirdBtn').show();
    $('#editBirdBtn').hide();
    $('#historyGroup').hide();
    $('.modalText').hide();

}
function unSwitchViewBird() {
    $("#birdModal input").hide();
    $("#birdModal select").hide();
    $("#birdModal .radio-list").hide();
    $('#updateBirdBtn').hide();
    $('#returnBirdBtn').hide();
    $('#editBirdBtn').show();
    $('#historyGroup').show();
    $('.modalText').show();
}

  $('#sexingMethodeSelect').on('change',function (e) {
        console.log(e.target.value);
        e.target.value !== 'unknow' ?  $('#genderGroup').css('display','block') : $('#genderGroup').css('display','none');
  });

$( "#UpdateBirdForm" ).submit(function(e) {
    e.preventDefault();
    let data =$( this ).serializeArray() ;
    console.log(data);
    updateBird(data);
});


function updateBird(data) {
    $.post( "/ajax/setBird",data).done(function( res ) {
        console.log(res);
        location.reload();
    });
}

$('#closeModalBtn').on('click',function (e) {
    e.preventDefault();
    console.log('close');
    unSwitchViewBird();
});

$('#returnBirdBtn').on('click',function () {
    unSwitchViewBird();
});

$('#birdModal').on('hidden.bs.modal', function (e) {
   unSwitchViewBird();
});


/********************
 *
 editImage
 *
 * *************************/


Dropzone.options.birdPictureDropzone = {
    init: function() {
        this.on("processing", function(file) {
            this.options.url = "/app/uploadBirdImage/"+birdId;
        });
        this.on("success", function(file, response) {
            console.log(file);
            //add thumbnails
            $('.thumbnailsGroup').append('<div class="img-wrap">'+
                '<a class="close deleteImage" data-toggle="tooltip" title="'+Lang.get("alerts.frontend.editBird")+'"  data-placement="bottom" id="delbtn'+response['imageId']+'">'+
                    '<i class="fa fa-times-circle-o"></i>'+
                '</a>'+
                '<img id="img'+response['imageId']+'" class="d-block img-fluid" src="/images/users/'+response['userId']+'/birds/'+birdId+'/'+response['name']+'" alt="First Image">'+
                '</div>');
            //addSlide
            $('#innerSlide').append('<div class="carousel-item " id="item'+response['imageId']+'">\n' +
                '                                    <img class="d-block img-fluid" src="/images/users/'+response['userId']+'/birds/'+birdId+'/'+response['name']+'" alt="First slide">' +
                '                                </div>');
            this.removeAllFiles();
            $('#item00').remove();


        });
    },
    dictDefaultMessage:Lang.get("labels.frontend.settings.dropZoneDefaultMessage"),
    maxFilesize: 5,
//     dictDefaultMessage:Lang.get("labels.frontend.settings.dropZoneDefaultMessage"),
    renameFile: function(file){
        var dt = new Date();
        var time = dt.getTime();
        return "birdImage_"+time+"."+file.name.split('.')[0];
    },
    acceptedFiles: ".jpeg,.jpg,.png,.gif",
    addRemoveLinks: true,
};


$('#editPicturesBtn').on('click',function (e) {
    $('#editPictureIcon').toggleClass('fa-pencil fa-rotate-left');
    // $('#innerSlide').fadeToggle();
    $('.editImage').fadeToggle();
});

