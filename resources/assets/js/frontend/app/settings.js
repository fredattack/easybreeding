
function checkUrl() {
    // url = new URL(window.location.href);
    let urlParams = new URLSearchParams(location.search);

    if (urlParams.has('target')) {
       if(urlParams.get('target')=='viewCal')$('#collapseCalendar').collapse('show');
       if(urlParams.get('target')=='viewProfile')$('#collapseUser').collapse('show');
    }

}


$(document).ready(function () {
    checkUrl();
});

/***************************
 *      Users Profile
 ****************************/


$('#editProfilBtn').on('click',function (e) {
    e.preventDefault();
    $('.profilText').toggle();
    $('.profilInput').toggle();
    // $('#returnProfilBtn').css('display','grid');
    $('#updateProfilBtn').toggle();
    $('#btnIcon').toggleClass('fa-pencil fa-rotate-left');
    $('#to-recover').toggle();

});

$('#returnProfilBtn').on('click',function (e) {
    e.preventDefault();
    $('.profilText').toggle();
    $('.profilInput').toggle();
    $('#returnProfilBtn').toggle();
    $('#updateProfilBtn').toggle();
    $('#editProfilBtn').toggle();
    $('#to-recover').toggle();
});

$('#to-recover').on('click',function (e) {
    $('#to-recover').toggleClass('activate');
    ($('#to-recover').hasClass('activate'))? $('#to-recover').html("<i class='fa fa-rotate-left m-r-5'></i>" +
        Lang.get("labels.frontend.calendar.cancel")): $('#to-recover').html("<i class='fa fa-lock m-r-5'></i>"+Lang.get("labels.frontend.passwords.reset_password_button"));
    $('#recoverpasswd').fadeToggle();
});


/********************
*
DropZone
*
 * *************************/
Dropzone.options.myDropzone = {
    init: function() {
        this.on("success", function(file, response) {
            console.log('succes');
            console.log(file);
            console.log(response);
            location.reload(true);
        });
    },
    maxFilesize: 5,
    dictDefaultMessage:Lang.get("labels.frontend.settings.dropZoneDefaultMessage"),
    renameFile: function(file) {
        var dt = new Date();
        var time = dt.getTime();
        return time+file.name;
    },
    acceptedFiles: ".jpeg,.jpg,.png,.gif",
    addRemoveLinks: true,
    timeout: 5000,

};


$('#deletePictureBtn').on('click',function (e) {
    let id= $(this).val();
    $.get('/ajax/deletePictureUser?id=' + id , function (data) {
        console.log('changed');
        location.reload();
    });

});

$('#editPictureBtn').on('click',function (e) {
    $('.dropCard').toggle();
    $('.imageZone').toggle();
    $('#editPictureIcon').toggleClass('fa-pencil fa-rotate-left');
});


/********************************************
 * Description: display create Category on button click
 * Parameters: none
 * Return
 *********************************************/

$('#addCategoryBtn2').on('click',function () {
    displayNewCatGroup2();
});



function displayNewCatGroup2(){
    $('.categoryGroup').fadeToggle();
    $('#addGroupCat').toggleClass('addCatGroup addCatGroupFlex');
    $('#addCategoryIcon').toggleClass('fa-plus').toggleClass('fa-rotate-left');
}



$('.categoryColorInput').on('change',function () {
    let idCat=$(this).prop('id').substr(4);
    let color=$('#'+$(this).prop('id')).spectrum('get').toHexString().slice(1);


    updateCategory(idCat,color);
});

function updateCategory(id,color) {

    $.get('/ajax/updateCategoryColor?id=' + id + '&color=' + color, function (data) {
        console.log('changed');
    });
}

$('.deleteCat').on('click',function () {
    let idCat=$(this).prop('id').substr(4);
    $.get('/ajax/deleteCategory?id=' + idCat , function (data) {
        $('#row'+idCat).toggle();
    });
});


/********************************************
 * Description: click on new category Button
 * Parameters: none
 * Return none
 *********************************************/
$('#saveCategoryset').on('click',function () {
    if($('#newCategoryInput').val()==null) $('.form-control-feedback').css('display','block');
    else {
        let color = $("#full").spectrum('get').toHexString();
        let title = $('#newCategoryInput').val();
        createCategorySet(title, color);
    }
});

/********************************************
 * Description: create category - sending to server with ajax
 * Parameters: title, val(color)
 * Return
 *********************************************/
function createCategorySet(title,val) {

    //remove the # from color to pass the query parameters in url
    let color = val.slice( 1 );

    $.get('/ajax/createCategory?title='+title+'&color='+color,function (data) {

        location.reload();

    });
}



$("input[name='defaultView']").on('change',function () {
    let val=$("input[name='defaultView']:checked").val();
    console.log('val= ' + val);
    updateDefaultView(val);
});

function updateDefaultView(val){
    $.get('/ajax/updateDefaultView?val='+val,function (data) {
        console.log(data);
    });
}


