<?php
return[
    "defaultMessage"=>     "Deze waarde lijkt ongeldig te zijn.",
    "type"=> [
        "email"=>          "Deze waarde moet een geldige e-mail zijn.",
        "url"=>            "Deze waarde moet een gevalideerde url zijn.",
        "number"=>         "Deze waarde moet een geldig nummer zijn.",
        "integer"=>        "Deze waarde moet een geldig geheel getal zijn.",
        "digits"=>         "Deze waarde moeten cijfers zijn.",
        "alphanum"=>       "Deze waarde moet alfanumeriek zijn."
    ],
    "notblank"=>           "Deze waarde moet niet leeg zijn.",
    "required"=>           "Deze waarde is vereist.",
    "pattern"=>            "Deze waarde lijkt ongeldig te zijn.",
    "min"=>                "Deze waarde moet groter zijn dan of gelijk aan% s.",
    "max"=>                "Deze waarde moet lager zijn dan of gelijk aan% s.",
    "range"=>              "Deze waarde moet tussen% s en% s zijn.",
    "minlength"=>          "Deze waarde is te kort, deze moet% s tekens of meer bevatten.",
    "maxlength"=>          "Deze waarde is te lang, het moet% s tekens of minder hebben.",
    "length"=>             "Deze waarde is ongeldig, deze moet tussen% s en% s tekens lang zijn.",
    "mincheck"=>           "U moet ten minste% s keuzes selecteren.",
    "maxcheck"=>           "U moet% s keuzes of minder selecteren.",
    "check"=>              "U moet kiezen tussen% s en% s keuzes.",
    "equalto"=>            "Deze waarde zou hetzelfde moeten zijn."
];