<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Labels Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in labels throughout the system.
    | Regardless where it is placed, a label can be listed here so it is easily
    | found in a intuitive way.
    |
    */

    'general' => [
        'delete' => 'Verwijder',
        'all'     => 'Alle',
        'yes'     => 'Ja',
        'no'      => 'Nee',
        'custom'  => 'Aangepast',
        'actions' => 'Acties',
        'active'  => 'Active',
        'buttons' => [
            'save'   => 'Bewaar',
            'update' => 'Bijwerken',
        ],
        'hide'              => 'Verberg',
        'inactive'          => 'Inactive',
        'none'              => 'Geen',
        'show'              => 'Toon',
        'toggle_navigation' => 'Navigatie omschakelen',
        'submit'    => 'voorleggen,'
    ],

    'backend' => [
        'access' => [
            'roles' => [
                'create'     => 'Rol Creëren',
                'edit'       => 'Rol Aanpassen',
                'management' => 'Rol Beheer',

                'table' => [
                    'number_of_users' => 'Aantal Gebruikers',
                    'permissions'     => 'Permissies',
                    'role'            => 'Rol',
                    'sort'            => 'Sorteer',
                    'total'           => 'rol|rollen',
                ],
            ],

            'users' => [
                'active'              => 'Actieve Gebruikers',
                'all_permissions'     => 'Alle Permissies',
                'change_password'     => 'Wachtwoord veranderen',
                'change_password_for' => 'Wachtwoord veranderen voor :user',
                'create'              => 'Gebruiker Aanmaken',
                'deactivated'         => 'Gedeactiveerde Gebruikers',
                'deleted'             => 'Verwijderde Gebruikers',
                'edit'                => 'Gebruiker aanpassen',
                'management'          => 'Gebruikers Beheer',
                'no_permissions'      => 'Geen Permissie',
                'no_roles'            => 'Geen rollen beschikbaar.',
                'permissions'         => 'Permissies',

                'table' => [
                    'confirmed'      => 'Bevestigd',
                    'created'        => 'Gecreëerd',
                    'email'          => 'E-mail',
                    'id'             => 'ID',
                    'last_updated'   => 'Laatst Bijgewerkt',
                    'first_name'     => 'Voornaam',
                    'last_name'      => 'Achternaam',
                    'name'           => 'Naam',
                    'no_deactivated' => 'Geen gedeactiveerde Gebruikers',
                    'no_deleted'     => 'Geen Verwijderde Gebruikers',
                    'roles'          => 'Rollen',
                    'social' => 'Social',
                    'total'          => 'gebruiker|gebruikers',
                ],

                'tabs' => [
                    'titles' => [
                        'overview' => 'Overzicht',
                        'history'  => 'Geschiedenis',
                    ],

                    'content' => [
                        'overview' => [
                            'avatar'       => 'Avatar',
                            'confirmed'    => 'Bevesstigd',
                            'created_at'   => 'Gecreëerd',
                            'deleted_at'   => 'Verwijdert',
                            'email'        => 'E-mail',
                            'last_updated' => 'Laatst bijgewerkt',
                            'name'         => 'Naam',
                            'status'       => 'Status',
                        ],
                    ],
                ],

                'view' => 'Bekijk gebruiker',
            ],
        ],
    ],
    'frontend' => [
        'date' => [
            'year' =>       'Jaar',
            'years' =>      'Jaren',
            'month' =>      'Maand',
            'week' =>       'Weekend',
            'weeks' =>      'Weeks',
            'day' =>        'Day',
            'today' =>      "Vandaag",
            'days' =>       'Dads',
            'list' =>       'Lijst',
            'months' => [
                "January" =>        "Januari",
                "February" =>       "Februari",
                "March" =>          "Maart",
                "April" =>          "April",
                "May" =>            "Mei",
                "June" =>           "Juni",
                "July" =>           "Juli",
                "August" =>         "August",
                "September" =>      "September",
                "October" =>        "Oktober",
                "November" =>       "November",
                "December" =>       "December",
            ],
        ],
        'calendar' => [

                'colorCategory' =>  'Stel de kleur in',
                'categories' =>     'Categorieën' ,
                'saveCategory' =>   'Bewaar deze categorie',
                'chooseCategory' => 'Kies een categorie',
                'AddNewCategory' => 'Categorie toevoegen',
                'AddNewEvent' =>    'Voeg een evenement toe',
                'updateEvent' =>    'Bewerk deze gebeurtenis',
                'cancel' =>         'Annuleren'  ,
                'allDay' =>         'De hele dag',
                'addTitle' =>       'Voeg een titel toe',
                'title' =>          'Titel' ,
                'mm/dd/yyyy' =>     "Dd / mm / jjjj",
            'name' =>               'Naam' ,
            'defaultView' =>        'Standaard weergeven',
            'default' =>            'Standaard',
            'laying' =>             'Plaatsen',
            'nestling' =>           'Hatching',
            'controlFecundity' =>   'Vruchtbaarheidscontrole',
            'googleAccount' =>      'Google-account',
            'googleInfo' =>         'U hebt de mogelijkheid om uw EasyBreeding-agenda te synchroniseren met uw Google-kalender.
                                    Voer hiertoe uw e-mailadres "@ gmail.com" in en volg de instructies op het scherm. '
        ],


        'auth' => [
            'login_box_title' =>   'Login',
            'login_button' =>       'Login',
            'login_with' =>         'Inloggen met: social_media',
            'register_box_title' =>'Register',
            'register_button' =>   'Register',
            'remember_me' =>       'Remember Me',
            'signIn' =>             'Connect',
            'noAccount' =>          'Nog geen account?',
            'signUp' =>             "Register",


        ],

        'contact' => [
            'box_title' =>  'Neem contact met ons op',
            'button' =>     'Verzend informatie',
        ],

        'passwords' => [
                'expired_password_box_title' => 'Uw wachtwoord is verlopen',
                'forgot_password' =>            'Wachtwoord vergeten?',
                'reset_password_box_title' =>   'Reset wachtwoord',
                'reset_password_button' =>      'Reset wachtwoord',
                'update_password_button' =>     'Wachtwoord bijwerken',
            'send_password_reset_link_button' => 'Stuur Wachtwoord Reset Link',
        ],

        'user' => [
            'passwords' => [
                'change' => 'Wachtwoord wijzigen',
                'updated' =>'Het wachtwoord is bijgewerkt'

            ],

            'profile' => [
                'avatar' =>            'Avatar' ,
                'created_at' =>        'Gemaakt bij',
                'edit_information' => 'Informatie bewerken ',
                'email' =>            'E-mail' ,
                'last_updated' =>     'Laatst bijgewerkt',
                'name' =>             'Naam',
                'first_name' =>       'Voornaam',
                'last_name' =>        'Achternaam',
                'update_information' =>'Informatie bijwerken',
            ],
        ],
        //@todo multi language after this
        'birds' => [
            'order' =>         'Order',
            'species' =>      'Species',
            'familly' =>      'Family',
            'usualName' =>    'Algemene naam',
            'gender' =>       'Sex',
            'male' =>          'Man',
            'female' =>        'Pop',
            'unknow' =>        'Unknown',
            'sexingMethod' =>  'sexing-methode',
            'mutation' =>      'Mutation',
            'birthDate' =>     'Geboortedatum',
            'age' =>           'Age',
            'idType' =>        "Type ID",
            'idPerso' =>       'Aangepaste id',
            'id' =>            'Identificeren',
            'idNummer' =>      "ID-nummer",
            'origin' =>        "Oorsprong",
            'status' =>        'Status'    ,
            'disponibility' => 'Beschikbaarheid',
            'history' =>       'History',
            'mother' =>        'Mother',
            'father' =>        'Vader',
            'orderFirst' =>    "Kies eerst een Order.",
            'famillyFirst' =>  'Kies een gezin first',
            'speciesFirst' =>  'Kies een soort first',
            'specieCustom' =>  'Kies een van uw Species',
            'openRings' =>     'Open Ring',
            'closedRings' =>   'Gesloten ring',
                'noOne' =>     'Geen' ,
                'other' =>     'Andere',
                'DNA' =>       'DNA' ,
            'endoscopy' =>     'Endoscopie',
            'supposed' =>      'Aangenomen' ,
            'dymorphism' =>    'Seksuele Dymorfie',
            'thisElevage' =>   'Eigen fokkerij',
            'advertencie' =>   'Kleine advertentie',
            'friend' =>        'Vriend - Contact',
            'expo' =>          'Expo - Stock Exchange',
            'infoBreeder' =>   'Fokker',
            'available' =>     'Beschikbaar',
            'toBeSale' =>      'Geven',
            'dead' =>          'Dood',
            'sold' =>          'Verkocht',
            'reserved' =>      'Gereserveerd',
            'coupled' =>       'In paar',
            'single' =>        'Only',
            'rest' =>          'In rust',
            'Incubation' =>    'incubatie time',
            'fertilityControl' => 'Fertility control',
            'girdleDate' =>    'Age to ring',
            'outOfNest' =>     'Uit het nest',
            'weaning' =>       'Spenen',
            'sexualMaturity' =>'Seksuele maturity',
            'spawningInterval' => 'Laying interval',
            'idCouples' =>     'Id. van paren',
            'couplesFrom' =>   'In koppel sinds',
            'until' =>         'Sindsdien gescheiden',
                'separateCouple'=>' Scheid het koppele',
            'hatchingsNbr' =>  'Nbr. Brood',
            'eggsNbr' =>       'Nbr. Eggs',
            'whiteEggsNbr' =>  'Nbr. Witte eieren',
            'fertilizedEggNbr' => 'Nbr. Fertilized eggs',
            'youngNbr' => 'Nbr. jongen',
                    'createCouples' =>  'Vorm een koppel',
                    'selectMale' =>     'Kies een man',
                    'selectFemale' =>   'Kies een vrouw',
                    'selectCouple' =>   'Kies een paar',
                    'filterBySpecie' => 'Filter op soorten',
                    'filterByStatus' => 'Filteren op status',
                    'filterByCouple' => 'Filter op paar',
            'filterByDisponibility' =>  'Filteren op beschikbaarheid',
            'filterBySexe' =>           'Filteren op geslacht',
            'all' =>                    'All',
            'filterByMale' =>           'Filter op mannelijk',
            'filterByFemale' =>         'Filter op vrouw',
            'eggsStat' =>               "Kweekstatistieken",
            'layingStat' =>             "broedstatistieken",
        ],
        'eggs' => [
            "layingDate" =>     "Datum van leggen",
            "position" =>       "Positie"   ,
            "eggState" =>       "Staat van het ei",
            "selectEggState" => "Kies de staat van het ei",
            "how" =>            "Note" ,
            "good" =>           "Goed",
            "dirty" =>          "Dirty",
            "flabby" =>         "Soft",
            "damaged" =>        "Beschadigde",
            "broken" =>         "Beschadigde",
            "nextStep" =>       "Volgende stap",
            "fertilized" =>     "Bevruchte",
            "white" =>          "Wit ei",
            "deadInEgg" =>      "Dood in het ei",
            "hatchingDate" =>   "broeddatum",
            "birdHached" =>     "Vogel uitgebroed",
            "hached" =>         "gearceerde vogel",
            "birdNotHached" =>  "Ei niet uitgebroed",
            "abandoned" =>      "Verlaten door ouders",
            "unknow" =>         "Onbekend",
    "selectReasonNotHatched" => "Oorzaak",
            "hatched" =>        "Locked",
            "hatching" =>       "Hatching",
            "eatByParent" =>    "Eet door ouders"

        ] ,
        'hatchings' => [
            'start' =>          'Start',
            'end' =>            'End',
            'status' =>         'Status',
            'still' =>          'In uitvoering',
            'finish' =>         'Klaar',
            'eggsCount' =>      'Nbr. Eggs',
        'whiteCount' =>         'Witte eieren',
            'flabbyCount' =>    'Zachte eieren',
            'brokenCount' =>    'Eieren gebroken',
            'hatchedCount' =>   'Eieren uitgebroed',
            'deadCount' =>      'Death In The Egg',
            'abandonedCount' => 'Verlaten door ouders',
            'eatCount' =>       'Gedood door ouders',
        ],
        'ZoneAndCage' => [
            'name' =>           'Naam',
            'cage' =>           'Kooie',
            'long' =>           'Lengte',
            'wide' =>           'Breedte',
            'height' =>         'Hoogte',
            'zone' =>           'Zone',
            'description' =>    'Beschrijving',
        ],
        'table' => [
            "sProcessing" =>    "Bezig met verwerken ...",
            "sSearch" =>           "Zoeken&nbsp;:",
            "sLengthMenu" =>      "Toon _MENU_ artikelen",
            "sInfo" =>             "Weergave van het _START_ item _END_on _TOTAL_ items",
            "sInfoEmpty" =>       "Weergave van de gebeurtenis 0 op 0",
            "sInfoFiltered" =>    "(filter van _MAX_in totaal)",
            "sInfoPostFix" =>     "",
            "sLoadingRecords" =>  "Bezig met laden ...",
            "sZeroRecords" =>     "Geen nog te tonen",
            "sEmptyTable" =>      "Geen gegevens beschikbaar in de tabel",
            "sFirst" =>           "Eerst",
            "sPrevious" =>         "Vorige",
            "sNext" =>            "Volgende",
            "sLast" =>             "Last",
            "sSortAscending" =>    ": activeren om de kolom in oplopende volgorde te sorteren",
            "sSortDescending" =>  ": activeren om de kolom in oplopende volgorde te sorteren",
            'rowsX' =>            "% d rijen geselecteerd",
            'rows0' =>            "Geen geselecteerde lijn",
            'rows1' =>            "1 geselecteerde regel",

        ],
        'settings' => [
            'userProfile' =>              "Gebruikersprofiel",
            'general' =>                 "Algemene instellingen",
            'calendar' =>                "Agenda-instellingen",
            'dropZoneDefaultMessage' =>  'Klik hier of voeg een foto toe om een ​​profielfoto toe te voegen'
        ],
        'birdsInBreed' =>   'Aantal vogels in de fokkerij',
        'eggssInNest' =>    "Aantal eieren in de nesten",
        'birdsInNest' =>    "Aantal jongen op nest",
    ],


    
    
    
];
