<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Alert Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain alert messages for various scenarios
    | during CRUD operations. You are free to modify these language lines
    | according to your application's requirements.
    |
    */

    'backend' => [
        'roles' => [
            'created' => 'De rol is succesvol aangemaakt.',
            'deleted' => 'De rol is succesvol verwijderd.',
            'updated' => 'De rol is succesvol bijgewerkt.',
        ],

        'users' => [
            'cant_resend_confirmation' => 'The application is currently set to manually approve users.',
            'confirmation_email'  => 'Een nieuwe bevestigings e-mail is verzonden naar het aangegeven adres.',
            'confirmed'              => 'The user was successfully confirmed.',
            'created'             => 'De gebruiker is succesvol aangemaakt.',
            'deleted'             => 'De gebruiker is succesvol verwijderd.',
            'deleted_permanently' => 'De gebruiker is permanent verwijderd.',
            'restored'            => 'De gebruiker is met succes hersteld.',
            'session_cleared'      => "The user's session was successfully cleared.",
            'social_deleted' => 'Social Account Successfully Removed',
            'unconfirmed' => 'The user was successfully un-confirmed',
            'updated'             => 'De gebruiker is succesvol bijgewerkt.',
            'updated_password'    => 'Het wachtwoord van de gebruiker is succesvol bijgewerkt',
        ],
    ],

    'frontend' => [
        'contact' => [
            'sent' => 'Your information was successfully sent. We will respond back to the e-mail provided as soon as we can.',
        ],

        'addBird'    => 'Voeg een vogel toe',
        'displayList'    => 'Lijstweergave',
        'displayBlock'    => 'Blokweergave',
        'noBirds'    => "Je hebt nog geen vogel toegevoegd.",
        'from' => 'Vanaf',
        'to' => 'Tot',
        'close' => 'Sluit de windows',
        'addCouples' => 'Voeg toe aan een paar',
        'addCouple' => 'Koppelen',
        'addEgg' => 'Voeg een ei toe',
        'editBird' => "Bewerk de vogel",
        'editSpecie' => "Bewerk deze soort",
        'editProfile' => "Pas je profiel aan",
        'createSpecie' => "Voeg een ei toe soort",
        'SelectCustom' => "Selecteer een van uw soorten",
        'searchSpecie' => "Zoek een soort ...",
        'noCouples' => "Je hebt nog geen koppel toegevoegd",
        'noHatchings'    => "You have not added any hatching yet.",
        'viewSpecie' => "Zie het blad van de soort",
        'viewBird' => "Zie het vogelfiche",
        'birdUpdated' => "De vogel is bijgewerkt",
        'birdCreated' => "De vogel is toegevoegd",
        'goBack' => "Ga terug",
        'coupleCreated' => "Het paar is toegevoegd",
        'eggAdded' => "Het ei is toegevoegd",
        'coupleWellSeparate' => "Het paar is gescheiden",

        'separateCouple' => 'Scheid het paar',
        'viewcoupleDetails' => "Toon de gegevens van dit koppel",
        'viewBirdDetails' => "Toon de gegevens van dit vogel",
        'vieweggsDetails' => "Toon de gegevens van dit ei",
        "addEggs" => "Voeg een ei toe",
        "coupleHistory" => "Koppel geschiedenis",
        "viewHatching" => "Zie actieve arceringen",
        'noEggs' => "You have not added any egg yet",
        'nestlingDead' => 'Je hebt nog geen ei toegevoegd',
        'outOfNest' => 'Uit het nest',
        'dateRange' => 'Selecteer een datumbereik',
        'newZone' => 'Voeg een zone toe',
        'newCage' => 'Voeg een kooie toe',
        'editCage' => 'Bewerk deze kooi',
        'birdOutOfCage' => "Verwijder de vogel van deze kooi",
        'eggNum' => 'Ei n° ',
        'deleteEvent' => "Verwijder dit evenement",
        'dataMissing' => "Sommige gegevens, zoals de tijd van de incubatie of de tijd vóór de
                            vruchtbaarheidstest zijn niet ingevuld voor deze soort,
                             om het beste uit deze applicatie te halen, is het raadzaam om ze te informeren."

    ],
];
