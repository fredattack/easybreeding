<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Navs Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in menu items throughout the system.
    | Regardless where it is placed, a menu item can be listed here so it is easily
    | found in a intuitive way.
    |
    */

    'general' => [
        'home'   => 'StartPagina',
        'logout' => 'Uitloggen',
        'login' => 'Verbinding'
    ],

    'frontend' => [
        'contact' => 'Contact',
        'dashboard' => 'Dashboard',
        'birds' => 'de vogels',
        'login'     => 'Inloggen',
        'macros'    => 'Macros',
        'register'  => 'Registreer',

        'addbird' =>    'Voeg een vogel toe',
        'editbird' =>   "Bewerk een vogel",
        'editSpecie' => "Een soort bewerken",
        'couples'   => 'De koppelen',
        'eggs' =>       'Eggs',
        'hatchings' => 'The Broods',
        'nestlings' =>  'The Birds (in the nest) ',
        'zoneAndCage' => 'Zones en kooien',
        'tableSearch' =>'Zoeken in de tabel',



        'user' => [
            'account'         => 'Mijn Account',
            'administration'  => 'Beheer',
            'change_password' => 'Verander Wachtwoord',
            'my_information'  => 'Mijn Informatie',
            'profile'         => 'Profiel',
        ],
    ],
];
