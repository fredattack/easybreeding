<?php
 return[
     "defaultMessage"=>     "Dieser Wert scheint ungültig zu sein.",
    "type"=> [
      "email"=>             "Dieser Wert sollte eine gültige E-Mail sein.",
      "url"=>               "Dieser Wert sollte als URL validiert werden.",
      "number"=>            "Dieser Wert sollte eine gültige Zahl sein.",
      "integer"=>           "Dieser Wert sollte eine gültige Ganzzahl sein.",
      "digits"=>            "Dieser Wert sollte Ziffern sein.",
      "alphanum"=>          "Dieser Wert sollte alphanumerisch sein."
    ],
    "notblank"=>            "Dieser Wert sollte nicht leer sein.",
    "required"=>            "Dieser Wert ist erforderlich.",
    "pattern"=>             "Dieser Wert scheint ungültig zu sein.",
    "min"=>                 "Dieser Wert sollte größer oder gleich% s sein.",
    "max"=>                 "Dieser Wert sollte kleiner oder gleich% s sein.",
    "range"=>               "Dieser Wert sollte zwischen% s und% s liegen.",
    "minlength"=>           "Dieser Wert ist zu kurz, er sollte% s Zeichen oder mehr haben.",
    "maxlength"=>           "Dieser Wert ist zu lang, er sollte% s Zeichen oder weniger haben.",
    "length"=>              "Dieser Wert ist ungültig, er sollte zwischen% s und% s Zeichen lang sein.",
    "mincheck"=>            "Sie müssen mindestens% s auswählen.",
    "maxcheck"=>            "Sie müssen% s Auswahlmöglichkeiten oder weniger auswählen.",
    "check"=>               "Sie müssen zwischen% s und% s wählen.",
    "equalto"=>             "Dieser Wert sollte gleich sein."
 ];