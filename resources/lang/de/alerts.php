<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Alert Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain alert messages for various scenarios
    | during CRUD operations. You are free to modify these language lines
    | according to your application's requirements.
    |
    */

    'backend' => [
        'roles' => [
            'created' => 'Rolle erstellt.',
            'deleted' => 'Rolle gelöscht.',
            'updated' => 'Rolle aktualisiert.',
        ],

        'users' => [
            'cant_resend_confirmation' => 'The application is currently set to manually approve users.',
            'confirmation_email'  => 'Eine Aktivierungsmail wurde an die angegebene E-Mailadresse gesendet.',
            'confirmed'              => 'The user was successfully confirmed.',
            'created'             => 'Benutzer erstellt.',
            'deleted'             => 'Benutzer gelöscht.',
            'deleted_permanently' => 'Benutzer permanent gelöscht.',
            'restored'            => 'Benutzer wiederhergestellt.',
            'session_cleared'      => "The user's session was successfully cleared.",
            'social_deleted' => 'Social Account Successfully Removed',
            'unconfirmed' => 'The user was successfully un-confirmed',
            'updated'             => 'Benutzer aktualisiert.',
            'updated_password'    => 'Kennwort des Benutzers aktualisiert.',
        ],
    ],

    'frontend' => [
        'contact' => [
            'sent' => 'Your information was successfully sent. We will respond back to the e-mail provided as soon as we can.',
        ],
        'displayList'    => 'Listanzeige',
        'displayBlock'    => 'Blockanzeige',
        'addBird'    => 'Fügen Sie einen Vogel hinzu',
        'noBirds'    => "Du hast noch keinen Vogel hinzugefügt.",

        'from' => 'Von',
        'to' => 'Zu',
        'close' => 'Schließe die Fenster',

        'addCouples' => 'Zu einem Paar hinzufügen',
        'addCouple' => 'Kupplung',
        'addEgg' => 'Fügen Sie ein Ei hinzu',
        'editBird' => "Bearbeite den Vogel ",
        'editSpecie' => "Bearbeite diese Spezies",
        'editProfile' => "Bearbeite dein Profil",
        'createSpecie' => "Füge eine neue Spezies hinzu",
        'SelectCustom' => "Wähle eine deiner Spezies aus",
        'searchSpecie' => "Suche eine Spezies ...",

        'noCouples' => "Du hast noch kein Paar hinzugefügt",
        'viewSpecie' => "Siehe das Datenblatt der Spezies",
        'viewBird' => "Siehe das latt des Vogels",
        'birdUpdated' => "Der Vogel wurde aktualisiert",
        'birdCreated' => "Der Vogel wurde hinzugefügt",
        'goBack' => "Geh zurück",
        'coupleCreated' => "Das Paar wurde hinzugefügt",
        'eggAdded' => "Das Ei wurde hinzugefügt",
        'coupleWellSeparate' => "Das Paar wurde getrennt",

        'separateCouple' => 'Trenne das Paar',
        'viewcoupleDetails' => "Zeigen Sie die Details dieses Paares an",
        'viewBirdDetails' => "Zeigen Sie die Details dieser Vögel an",
        'vieweggsDetails' => "Zeigen Sie die Details dieses Eies an",
        "addEggs" => "Fügen Sie ein Ei hinzu",
        "coupleHistory" => "Paar Geschichte",
        "viewHatching" => "Siehe aktive Schraffuren",
        'noEggs' => "Du hast noch kein Ei hinzugefügt",
        'nestlingDead' => 'Der Nestling ist tot',
        'outOfNest' => 'Aus dem Nest',
        'dateRange' => 'Wählen Sie einen Datumsbereich aus',
        'newZone' => 'Fügen Sie eine Zone hinzu',
        'newCage' => 'Fügen Sie einen Käfig hinzu',
        'editCage' => 'Bearbeite diesen Käfig',
        'birdOutOfCage' => "Entferne den Vogel von diesem Käfig",
        'eggNum' => 'Ei n° ',
        'deleteEvent' => "Löschen Sie dieses Ereignis",
        'dataMissing' => "Einige Daten wie die Dauer der Inkubation oder die Dauer vor der
                            Fruchtbarkeitskontrolle sind für diese Spezies nicht ausgefüllt,
                             Um diese Anwendung optimal zu nutzen, ist es ratsam, sie zu informieren."

    ],
];
