<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Navs Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in menu items throughout the system.
    | Regardless where it is placed, a menu item can be listed here so it is easily
    | found in a intuitive way.
    |
    */

    'general' => [
        'home'   => 'Startseite',
        'logout' => 'Abmelden',
        'login' => 'Verbindung'
    ],

    'frontend' => [
        'contact' => 'Contact',
        'dashboard' => 'Dashboard',
        'birds' => 'die Vögel',
        'login'     => 'Anmelden',
        'macros'    => 'Makros',
        'register'  => 'Registrieren',
        'addbird' =>    'Füge einen Vogel hinzu',
        'editbird' =>   "Bearbeiten Sie einen Vogel",
        'editSpecie' => "Bearbeiten einer Spezies",
        'couples'   =>  "Paare",
        'eggs' =>       "Eier",
        'hatchings' =>  "Die Brut",
        'nestlings' =>  'Die Vögel (im Nest)',
        'zoneAndCage' => "Zonen und Käfige",
        'tableSearch' => 'Suche in der Tabelle',



        'user' => [
            'account'         => 'Mein Konto',
            'administration'  => 'Administration',
            'change_password' => 'Kennwort ändern',
            'my_information'  => 'Meine Informationen',
            'profile'         => 'Profile',
        ],
    ],
];
