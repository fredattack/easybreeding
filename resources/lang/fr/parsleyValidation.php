<?php
return[
    "defaultMessage"=>     "Cette valeur semble être invalide.",
    "type"=> [
    "email"=>              "Cette valeur doit être un email valide.",
    "url"=>                "Cette valeur doit être une URL valide.",
    "number"=>             "Cette valeur doit être un nombre valide.",
    "integer"=>            "Cette valeur doit être un entier valide.",
    "digits"=>             "Cette valeur doit être en chiffres.",
    "alphanum"=>           "Cette valeur doit être alphanumérique."
    ],
    "notblank"=>           "Cette valeur ne doit pas être vide.",
    "required"=>           "Cette valeur est requise.",
    "pattern"=>            "Cette valeur semble être invalide.",
    "min"=>                "Cette valeur doit être supérieure ou égale à% s.",
    "max"=>                "Cette valeur doit être inférieure ou égale à% s.",
    "range"=>              "Cette valeur doit être comprise entre% s et% s.",
    "minlength"=>          "Cette valeur est trop courte. Elle devrait avoir% s caractères ou plus.",
    "maxlength"=>          "Cette valeur est trop longue. Elle devrait contenir% s caractères au maximum.",
    "length"=>             "La longueur de cette valeur est invalide. Elle doit contenir entre% s et% s caractères.",
    "mincheck"=>           "Vous devez sélectionner au moins% s choix.",
    "maxcheck"=>           "Vous devez sélectionner% s choix ou moins.",
    "check"=>              "Vous devez choisir entre% s et% s choix.",
    "equalto"=>            "Cette valeur devrait être la même."
];