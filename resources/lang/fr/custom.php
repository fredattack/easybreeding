<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Custom Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'offers'        => 'Annonces',
    'agenda'        => 'Agenda',

    'birds'         => 'Oiseaux',
    'nestlings'         => 'Les oisillons (au nid)',
    'birdsList'         => 'Les Oiseaux',


    'couples'       => 'Couples',
    'hatchings'     => 'Couvées',
    'eggs'          => 'Oeufs',
    'zoneCages'     => 'Zones & Cages',
    'settings'      => 'Paramètres',
];
