<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Navs Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in menu items throughout the system.
    | Regardless where it is placed, a menu item can be listed here so it is easily
    | found in a intuitive way.
    |
    */

    'general' => [
        'home'   => 'Home',
        'logout' => 'Logout',
        'login' => 'Login'
    ],

    'frontend' => [
        'contact' => 'Contact',
        'dashboard' => 'Dashboard',
        'birds' => 'the Birds',
        'login'     => 'Login',
        'macros'    => 'Macros',
        'register'  => 'Register',

                'addbird' =>    'Add a bird',
                'editbird' =>   "Edit an Bird",
                'editSpecie' => "Edit a Species",
                'couples'   =>  'Couples',
                'eggs' =>       'Eggs',
                'hatchings' =>  'The hatchings',
                'nestlings' =>   'Nestlings (at the nest)',
                'zoneAndCage' =>'Zones and cages',
                'tableSearch' =>'Search in the table',

        'user' => [
            'account'         => 'My Account',
            'administration'  => 'Administration',
            'change_password' => 'Change Password',
            'my_information'  => 'My Information',
            'profile'         => 'Profile',
        ],
    ],
];
