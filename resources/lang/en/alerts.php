<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Alert Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain alert messages for various scenarios
    | during CRUD operations. You are free to modify these language lines
    | according to your application's requirements.
    |
    */

    'backend' => [
        'roles' => [
            'created' => 'The role was successfully created.',
            'deleted' => 'The role was successfully deleted.',
            'updated' => 'The role was successfully updated.',
        ],

        'users' => [
            'cant_resend_confirmation' => 'The application is currently set to manually approve users.',
            'confirmation_email' => 'A new confirmation e-mail has been sent to the address on file.',
            'confirmed' => 'The user was successfully confirmed.',
            'created' => 'The user was successfully created.',
            'deleted' => 'The user was successfully deleted.',
            'deleted_permanently' => 'The user was deleted permanently.',
            'restored' => 'The user was successfully restored.',
            'session_cleared' => "The user's session was successfully cleared.",
            'social_deleted' => 'Social Account Successfully Removed',
            'unconfirmed' => 'The user was successfully un-confirmed',
            'updated' => 'The user was successfully updated.',
            'updated_password' => "The user's password was successfully updated.",
        ],
    ],

    'frontend' => [
        'contact' => [
            'sent' => 'Your information was successfully sent. We will respond back to the e-mail provided as soon as we can.',
        ],

        'displayList' => 'DisplayList',
        'displayBlock' => 'DisplayBlock',
        'addBird' => 'Add a bird',
        'noBirds' => "You have not added any bird yet.",

        'from' => 'From',
        'to' => 'To',
        'close' => 'Close the windows',
        'addCouples' => 'Add to a couple',
        'addCouple' => 'Coupling',
        'addEgg' => 'Add an egg',
        'editBird' => "Edit the bird ",
        'editSpecie' => "Edit this species",
        'editProfile' => "Edit your profile",
        'createSpecie' => "Add a new specie",
        'SelectCustom' => "Select one of your species",
        'searchSpecie' => "Search a species ...",

        'noCouples' => "You have not added any couple yet",
        'viewSpecie' => "See the specie's sheet",
        'viewBird' => "See the bird's sheet",
        'birdUpdated' => "The bird has been updated",
        'birdCreated' => "The bird has been added",
        'goBack' => "Go back",
        'coupleCreated' => "The couple has been added",
        'eggAdded' => "The egg has been added",
        'coupleWellSeparate' => "The couple has been separeted",

        'separateCouple' => 'Separate the couple',
        'viewcoupleDetails' => "Display this couple's details",
        'viewBirdDetails' => "Display this birds's details",
        'vieweggsDetails' => "Display this egg's details",
        "addEggs" => "Add an egg",
        "coupleHistory" => "Couple history",
        "viewHatching" => "See active Hatchings",
        'noEggs' => "You have not added any egg yet",
        'noHatchings'    => "You have not added any hatching yet.",
        'nestlingDead' => 'The nestling is dead',
        'outOfNest' => 'Out of nest',
        'dateRange' => 'Select a date range',
        'newZone' => 'Add a zone',
        'newCage' => 'Add a cage',
        'editCage' => 'Edit this cage',
        'birdOutOfCage' => "Remove the bird of this cage",
        'eggNum' => 'Egg n° ',
        'deleteEvent' => "Delete this Event",
        'dataMissing' => "Some data such as the duration of incubation or the duration before the 
                           fertility check are not filled in for this species, 
                            in order to make the most of this application, it is advisable to inform them."


    ],
];
