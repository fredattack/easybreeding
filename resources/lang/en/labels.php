<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Labels Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in labels throughout the system.
    | Regardless where it is placed, a label can be listed here so it is easily
    | found in a intuitive way.
    |
    */

    'general' => [
        'delete' => 'Delete',
        'all' => 'All',
        'yes' => 'Yes',
        'no' => 'No',
        'copyright' => 'Copyright',
        'custom' => 'Custom',
        'actions' => 'Actions',
        'active' => 'Active',

        'buttons' => [
            'save' => 'Save',
            'update' => 'Update',
        ],

        'hide' => 'Hide',
        'inactive' => 'Inactive',
        'none' => 'None',
        'show' => 'Show',
        'toggle_navigation' => 'Toggle Navigation',
        'submit' => 'Submit',

    ],

    'backend' => [
        'access' => [
            'roles' => [
                'create' => 'Create Role',
                'edit' => 'Edit Role',
                'management' => 'Role Management',

                'table' => [
                    'number_of_users' => 'Number of Users',
                    'permissions' => 'Permissions',
                    'role' => 'Role',
                    'sort' => 'Sort',
                    'total' => 'role total|roles total',
                ],
            ],

            'users' => [
                'active' => 'Active Users',
                'all_permissions' => 'All Permissions',
                'change_password' => 'Change Password',
                'change_password_for' => 'Change Password for :user',
                'create' => 'Create User',
                'deactivated' => 'Deactivated Users',
                'deleted' => 'Deleted Users',
                'edit' => 'Edit User',
                'management' => 'User Management',
                'no_permissions' => 'No Permissions',
                'no_roles' => 'No Roles to set.',
                'permissions' => 'Permissions',

                'table' => [
                    'confirmed' => 'Confirmed',
                    'created' => 'Created',
                    'email' => 'E-mail',
                    'id' => 'ID',
                    'last_updated' => 'Last Updated',
                    'name' => 'Name',
                    'first_name' => 'First Name',
                    'last_name' => 'Last Name',
                    'no_deactivated' => 'No Deactivated Users',
                    'no_deleted' => 'No Deleted Users',
                    'other_permissions' => 'Other Permissions',
                    'permissions' => 'Permissions',
                    'roles' => 'Roles',
                    'social' => 'Social',
                    'total' => 'user total|users total',
                ],

                'tabs' => [
                    'titles' => [
                        'overview' => 'Overview',
                        'history' => 'History',
                    ],

                    'content' => [
                        'overview' => [
                            'avatar' => 'Avatar',
                            'confirmed' => 'Confirmed',
                            'created_at' => 'Created At',
                            'deleted_at' => 'Deleted At',
                            'email' => 'E-mail',
                            'last_updated' => 'Last Updated',
                            'name' => 'Name',
                            'first_name' => 'First Name',
                            'last_name' => 'Last Name',
                            'status' => 'Status',
                        ],
                    ],
                ],

                'view' => 'View User',
            ],
        ],
    ],

    'frontend' => [
        'date' => [
            'year' => 'Year',
            'years' => 'Years',
            'month' => 'Month',
            'week' => 'Week',
            'weeks' => 'Weeks',
            'day' => 'Day',
            'today' => "Today",
            'days' => 'Days',
            'list' => 'List',
            'months' => [
                "January" => "January",
                "February" => "February",
                "March" => "March",
                "April" => "April",
                "May" => "May",
                "June" => "June",
                "July" => "July",
                "August" => "August",
                "September" => "September",
                "October" => "October",
                "November" => "November",
                "December" => "December"
            ],
        ],
        'calendar' => [

            'colorCategory' =>  'Set the color',
            'categories' =>     'Categories',
            'saveCategory' =>   'Save this category',
            'chooseCategory' => 'Choose a category',
            'AddNewCategory' => 'Add category',
            'AddNewEvent' =>    'Add an event',
            'updateEvent' =>    'Edit this event',
            'cancel' =>         'Cancel',
            'allDay' =>         'All day',
            'addTitle' =>       'Add a title',
            'title' =>          'Title',
            'mm/dd/yyyy' =>     'Dd / mm / yyyy',
            'name' =>               'Name',
            'defaultView' =>        'View by default',
            'default' =>            'By default',
            'laying' =>             'Laying',
            'nestling' =>           'Hatching',
            'controlFecundity' =>   'Fertility control',
            'googleAccount' =>      'Google Account',
            'googleInfo' =>         'You have the option to synchronize your EasyBreeding calendar with your google calendar.
                                       To do this, enter your email address "@ gmail.com" and follow the instructions on the screen.'
        ],


        'auth' => [
            'login_box_title' => 'Login',
            'login_button' => 'Login',
            'login_with' => 'Login with :social_media',
            'register_box_title' => 'Register',
            'register_button' => 'Register',
            'remember_me' => 'Remember Me',
            'signIn' => 'Connect',
            'noAccount' => 'No account yet?',
            'signUp' => "Register",


        ],

        'contact' => [
            'box_title' => 'Contact Us',
            'button' => 'Send Information',
        ],

        'passwords' => [
            'expired_password_box_title' => 'Your password has expired.',
            'forgot_password' => 'Forgot Your Password?',
            'reset_password_box_title' => 'Reset Password',
            'reset_password_button' => 'Reset Password',
            'update_password_button' => 'Update Password',
            'send_password_reset_link_button' => 'Send Password Reset Link',
        ],

        'user' => [
            'passwords' => [
                'change' => 'Change Password',
                'updated' => 'The password has been updated'

            ],

            'profile' => [
                'avatar' => 'Avatar',
                'created_at' => 'Created At',
                'edit_information' => 'Edit Information',
                'email' => 'E-mail',
                'last_updated' => 'Last Updated',
                'name' => 'Name',
                'first_name' => 'First Name',
                'last_name' => 'Last Name',
                'update_information' => 'Update Information',
            ],
        ],
        //@todo multi language after this
        'birds' => [
        'order' => 'Order',
        'species' => 'Species',
        'familly' => 'Family',
        'usualName' => 'Common name',
        'gender' => 'Sex',
        'male' => 'Male',
        'female' => 'Female',
        'unknow' => 'Unknown',
        'sexingMethod' => 'sexing method',
        'mutation' => 'Mutation',
        'birthDate' => 'Birth Date',
        'age' => 'Age',
        'idType' => "Type of identifier",
        'idPerso' => 'Custom Id',
        'id' => 'Identifier',
        'idNummer' => "ID number",
        'origin' => "Origin",
        'status' => 'Status',
        'disponibility' => 'Availability',
        'history' => 'History',
        'mother' => 'Mother',
        'father' => 'Father',
        'orderFirst' => "First choose an Order.",
        'famillyFirst' => 'Choose a Family first',
        'speciesFirst' => 'Choose a species first',
        'specieCustom' => 'Choose one of your Species',
        'openRings' => 'Open Ring',
        'closedRings' => 'Closed Ring',
        'noOne' => 'None',
        'other' => 'Other',
        'DNA' => 'DNA',
        'endoscopy' => 'Endoscopy',
        'supposed' => 'Assumed',
        'dymorphism' => 'Sexual Dymorphism',
        'thisElevage' => 'Own breeding',
        'advertencie' => 'Small ad',
        'friend' => 'Friend - Contact',
        'expo' => 'Expo - Stock Exchange',
        'infoBreeder' => 'Breeder',
        'available' => 'Available',
        'toBeSale' => 'To give',
        'dead' => 'Dead',
        'sold' => 'Sold',
        'reserved' => 'Reserved',
        'coupled' => 'In couple',
        'single' => 'Only',
        'rest' => 'At rest',
        'Incubation' => 'Incubation time',
        'fertilityControl' => 'Fertility control',
        'girdleDate' => 'Age to ring',
        'outOfNest' => 'Out of the nest',
        'weaning' => 'weaning',
        'sexualMaturity' => 'Sexual maturity',
        'spawningInterval' => 'Laying interval',
        'idCouples' =>' id. of couples',
        'couplesFrom' => 'In couple since',
        'until' => 'Separated since',
        'separateCouple' => 'Separate the couple',
        'hatchingsNbr' => 'Nbr. broods',
        'eggsNbr' => 'Nbr. Eggs',
        'whiteEggsNbr' => 'Nbr. White eggs,',
        'fertilizedEggNbr' => 'Nbr. Fertilized eggs',
        'youngNbr' => 'Nbr. youth',
        'createCouples' => 'Form a Couple',
        'selectMale' => 'Choose a Male',
        'selectFemale' => 'Choose a Female',
        'selectCouple' => 'Choose a Couple',
        'filterBySpecie' => 'Filter by Species',
        'filterByStatus' => 'Filter by Status',
        'filterByCouple' => 'Filter by Couple',
        'filterByDisponibility' => 'Filter by Availability',
        'filterBySexe' => 'Filter by Sex',
        'all' => 'All',
        'filterByMale' => 'Filter by Male',
        'filterByFemale' => 'Filter by female',
        'eggsStat' => "Spawning statistics",
        'layingStat' => "hatching statistics",
        ],
        'eggs' => [
                "layingDate" => "Date of laying",
                "position" => "Position",
                "eggState" => "State of the egg",
                "selectEggState" => "Choose the state of the egg",
                "how" => "Note",
                "good" => "Good",
                "dirty" => "dirty",
                "flabby" => "Mou",
                "damaged" => "Damaged",
                "broken" => "Damaged",
                "nextStep" => "Next step",
                "fertilized" => "fertilized",
                "white" => "White egg",
                "deadInEgg" => "Dead in the egg",
                "hatchingDate" => "hatching date",
                "birdHached" => "Bird hatched",
                "hached" => "hatched bird",
                "birdNotHached" => "Egg not hatched",
                "abandoned" => "Abandoned by parents",
                "unknow" => "Unknown",
                "selectReasonNotHatched" => "Cause",
                "hatched" => "locked",
                "hatching" => "hatching",
                "eatByParent" => "Eat by parents"

        ] ,
        'hatchings' => [
        'start' => 'Start',
        'end' => 'End',
        'status' => 'Status',
        'still' => 'In progress',
        'finish' => 'Finished',
        'eggsCount' => 'Nbr. Eggs',
        'whiteCount' => 'White eggs',
        'flabbyCount' => 'Soft eggs',
        'brokenCount' => 'Eggs broken',
        'hatchedCount' => 'Eggs hatched',
        'deadCount' => 'Death In the egg',
        'abandonedCount' => 'Abandoned by parents',
        'eatCount' => 'Killed by parents',
        ],
        'ZoneAndCage' => [
        'name' => 'Name',
        'cage' => 'cage',
        'long' => 'Length',
        'wide' => 'Width',
        'height' => 'Height',
        'zone' => 'Zone',
        'description' => 'Description',

        ],
        'table' => [
            "sProcessing" => "Processing in progress ...",
            "sSearch" => "Search&nbsp;:",
            "sLengthMenu" => "Show _MENU_ items",
            "sInfo" => "Display of the _START_ item _END_ on _TOTAL_ items",
            "sInfoEmpty" => "Display of the event 0 on 0",
            "sInfoFiltered" => "(filter of _MAX_in total)",
            "sInfoPostFix" => "",
            "sLoadingRecords" => "Loading ...",
            "sZeroRecords" => "None yet to display ",
            "sEmptyTable" => "No data available in the table",

            "sFirst" => "First",
            "sPrevious" => "Previous",
            "sNext" => "Next",
            "sLast" => "Last",

        "sSortAscending" => ": activate to sort the column in ascending order",
        "sSortDescending" => ": activate to sort the column in ascending order",

        'rowsX' => "% d rows selected",
        'rows0' => "No selected line",
        'rows1' => "1 selected line"

        ],
        'settings' => [
         'userProfile' => "User profile",
         'general' => "General settings",
         'calendar' => "Calendar settings",
         'dropZoneDefaultMessage' => 'Click here or Add your picture to add a profile picture'
        ],
        'birdsInBreed' => "Number of birds in the breeding",
        'eggssInNest' => "Number of eggs in the nests",
        'birdsInNest' => "Number of young at nest",
    ],


];
